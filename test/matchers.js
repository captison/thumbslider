var addMatchers = require('add-matchers');


var matchers =
{
    toBeASet: (a) => typeof a.find((c,i) => a.slice(0, i).concat(a.slice(i+1)).indexOf(c) >= 0) === 'undefined',
    toContain: (e, a) => a.indexOf(e) >= 0
}

addMatchers(matchers);
