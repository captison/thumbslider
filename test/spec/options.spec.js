var { options } = require('../imports');


describe('Options configuration', function()
{
    it('default values must pass validation', function()
    {
        expect(() => options.check(options.defaults)).not.toThrowAnyError();
    });
});
