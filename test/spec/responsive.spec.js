var { options: { defaults }, responsive, utils } = require('../imports');
var { options } = require('../fixtures');


describe('The responsive configuration', function()
{
    beforeAll(function()
    {
        this.node = utils.create('div', { style: 'width: 640px;' });
        this.options = options();
        this.ro = responsive(Object.assign({}, defaults, this.options), this.node);
    });

    it('must allow for option values selected based on container dimensions', function()
    {
        expect(this.ro.selectorDisplay()).toBe('transient');
    });

    it('must pass through original value for non-responsive options', function()
    {
        expect(this.ro.responsive()).toBe(this.options.responsive);
    });
});
