var { utils } = require('../imports');


describe('The utils', function()
{
    describe('element creator', function()
    {
        it('must create a dom element', function()
        {
            var elem = utils.create('div');

            expect(elem.getAttribute).toBeFunction();
        });

        it('must create a dom element with attributes', function()
        {
            var elem = utils.create('div', { title: 'test element' });

            expect(elem.getAttribute('title')).toBe('test element');
        });
    });

    describe('class prefixer', function()
    {
        it('prefixes classnames appropriately', function()
        {
            var prefix = utils.vars.cnPrefix;

            var executed = utils.cnp('my-class Other-Class hidden');
            var expected = `${prefix}my-class ${prefix}Other-Class ${prefix}hidden`;

            expect(executed).toBe(expected);
        });
    });

    describe('attribute prefixer', function()
    {
        it('prefixes attribute names appropriately', function()
        {
            var prefix = utils.vars.cnPrefix;

            var executed = utils.anp('name title a-b-c');
            var expected = `data-${prefix}name data-${prefix}title data-${prefix}a-b-c`;

            expect(executed).toBe(expected);
        });
    });

    describe('object processor', function()
    {
        var object = { blue: 4, green: 7, yellow: 9, orange: 2 };
        var processed = utils.process(object, p => p.split('').reverse().join(''), v => v * 10);

        it('allows for manipulation of object property names', function()
        {
            expect(processed.eulb).toBeDefined();
            expect(processed.egnaro).toBeDefined();
        });

        it('allows for manipulation of object property values', function()
        {
            expect(processed.neerg).toBe(70);
            expect(processed.wolley).toBe(90);
        });
    });

    describe('shuffler', function()
    {
        var arrayOne = ['beagle', 'lion', 'sparrow', 'walrus', 'trout', 'alligator'];
        var arrayTwo = [...arrayOne];
        var shuffleOne = utils.shuffle(arrayOne);
        var shuffleTwo = utils.shuffle(arrayTwo);

        it('reorders arrays in place', function()
        {
            expect(shuffleOne).toBe(arrayOne);
            expect(shuffleTwo).toBe(arrayTwo);
        });

        it('maintains original array size', function()
        {
            expect(shuffleOne).toBeArrayOfSize(arrayTwo.length);
            expect(shuffleTwo).toBeArrayOfSize(arrayOne.length);
        });

        it('reorders items in an array', function()
        {
            expect(shuffleOne).not.toEqual(arrayTwo);
            expect(shuffleTwo).not.toEqual(arrayOne);
        });
    });

    describe('value swapper', function()
    {
        it('must swap array elements in place within an array', function()
        {
            var array = ['bryan', 'cody', 'bill', 'ben', 'phoenix'];

            var executed = utils.swap(array, 0, 3);
            var expected = ['ben', 'cody', 'bill', 'bryan', 'phoenix'];

            expect(executed).toEqual(expected);
        });
    });

    describe('unique id maker', function()
    {
        it('must create unique ids every time it is called', function()
        {
            var ids = [utils.uid(), utils.uid(), utils.uid(), utils.uid(), utils.uid()];

            expect(ids).toBeASet();
        });
    });

    describe('value sorter', function()
    {
        it('must return an array of object keys in ascending value order', function()
        {
            var executed = utils.valueSort({ a: 76, b: 105, c: 81, d: 9, e: 33 });
            var expected = ['d', 'e', 'a', 'c', 'b'];

            expect(executed).toEqual(expected);
        });
    });
});
