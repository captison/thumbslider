var { app, utils: { vars: { document: { body } } } } = require('../imports');
var { headers } = require('../fixtures');


describe('The application', function()
{
    beforeAll(function()
    {
        body.appendChild(headers());
        this.headersCarousel = app.create('headers');
    });

    it('must supply tweeners', function()
    {
        expect(app.tweeners).toBeNonEmptyObject();
    });

    it('must supply the default tweener', function()
    {
        expect(app.tweeners.none).toBeFunction();
    });

    it('must create carousels without options specified', function()
    {
        expect(this.headersCarousel).toBeNonEmptyObject();
    });

    it('must return a created carousel by name', function()
    {
        expect(app.get('headers')).toBe(this.headersCarousel);
    });

    it('must return options for a carousel by name', function()
    {
        expect(app.options('headers')).toBeNonEmptyObject();
    });

    it('must return the list of names of all carousels', function()
    {
        expect(app.names()).toBeArrayOfStrings();
    });

    describe('carousel instances', function()
    {
        it('must return their indexed name', function()
        {
            expect(this.headersCarousel.getName()).toBe('headers');
        });
    });
});
