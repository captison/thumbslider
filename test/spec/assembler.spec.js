var { assembler, utils } = require('../imports');


describe('The carousel assembler', function()
{
    beforeEach(function()
    {
        this.assembler = assembler(this.nodes = {}).assemble(utils.create('div'));
    });

    describe('assembly functionality', function()
    {
        it('must create the necessary nodes', function()
        {
            var names = 'root carousel viewer view prev prevIcon next nextIcon slides selector select toggle ' +
                'toggleIcon thumbs focus';

            var expected = names.split(/\s+/).sort();
            var executed = Object.keys(this.nodes).sort();

            expect(executed).toEqual(expected);
        });
    });
});
