var { utils } = require('./imports');


exports.headers = function()
{
    var headers = utils.create('div', { id: 'headers', style: 'width:1024px;height:768px;' });

    headers.innerHTML =
    `
        <div data-ts-name="slideA"><h1>Slide Alpha</h1></div>
        <div data-ts-name="slideB"><h1>Slide Bravo</h1></div>
        <div data-ts-name="slideC"><h1>Slide Charlie</h1></div>
        <div data-ts-name="slideD"><h1>Slide Delta</h1></div>
        <div data-ts-name="slideE"><h1>Slide Echo</h1></div>
        <div data-ts-name="slideF"><h1>Slide Foxtrot</h1></div>
        <div data-ts-name="slideG"><h1>Slide Golf</h1></div>
        <div data-ts-name="slideH"><h1>Slide Hotel</h1></div>
        <div data-ts-name="slideI"><h1>Slide India</h1></div>
        <div data-ts-name="slideJ"><h1>Slide Juliet</h1></div>
        <div data-ts-name="slideK"><h1>Slide Kilo</h1></div>
        <div data-ts-name="slideL"><h1>Slide Lima</h1></div>
    `;

    return headers;
}

exports.options = function()
{
    var options =
    {
        activeClass: { xs: 'focus', def: 'active' },
        pauseOnMouseover: { sm: false, def: true },
        responsive: { xs: 480, sm: 768, md: 1024, lg: 1200 },
        rotateDuration: 0.8,
        rotateEasing: { md: 'linear', def: null },
        rotateInterval: 3,
        selectorDisplay: { xs: 'hidden', md: 'transient', def: 'visible' },
        selectorHideDelay: 0,
        selectorPosition: { sm: 'top',  def: 'bottom' },
        selectorSize: { sm: 0.18, def: 0.20 },
        showThumbContent: { sm: false, def: true },
        viewerOrientation: { sm: 'vertical', md: 'horizontal', lg: 'vertical', def: 'horizontal' },
    }

    return options;
}
