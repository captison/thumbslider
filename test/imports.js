
var source = (n) => require('../src/' + n);

exports.app = source('code/factory');
exports.options = source('code/options');
exports.utils = source('code/utils');

exports.assembler = source('code/carousel/assembler');
exports.responsive = source('code/carousel/responsive');
