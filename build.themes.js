var fs = require('fs');
var less = require('less');
var path = require('path');


var fail = (e) => { if (e) throw e; };
var themesInDir = path.join(__dirname, 'src', 'style', 'themes');
var themesOutDir = path.join(__dirname, 'dist', 'themes');

module.exports = function()
{
    // additional themes entry poins
    fs.readdirSync(themesInDir).forEach((filename) =>
    {
        if (filename.slice(-5) === '.less')
        {
            var name = filename.slice(0, -5);

            fs.readFile(path.join(themesInDir, filename), 'utf8', (error, data) =>
            {
                fail(error);

                var success = (css, ext) =>
                {
                    var cssName = ext === name ? name : name + '-' + ext;

                    if (!fs.existsSync(themesOutDir)) fs.mkdir(themesOutDir, fail);

                    fs.writeFile(path.join(themesOutDir, cssName) + '.css', css, 'utf8', fail);
                }

                [ name, 'default' ].forEach(x =>
                {
                    less.render(data, { modifyVars: { theme: x } }).then(o => success(o.css, x), fail);
                });
            });
        }
    });
}

module.exports();
