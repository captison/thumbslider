/*! thumbslider v0.0.4 @Mon, 24 Apr 2017 18:20:15 GMT */
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["thumbslider"] = factory();
	else
		root["thumbslider"] = factory();
})(this, function() {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 17);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var browser = __webpack_require__(6);

/*
    Application utility functions.

 */
module.exports = {
    // add attributes to element
    addAttrs: function addAttrs(e) {
        var a = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        for (var n in a) {
            e.setAttribute(n, n === 'class' ? this.cnp(a[n]) : a[n]);
        }return e;
    },


    // Add html attribute name prefix to `string`
    anp: function anp(s) {
        var _this = this;

        return s.split(/\s+/).map(function (n) {
            return 'data-' + _this.cnp(n);
        }).join(' ');
    },


    // convert item to array optionally slicing away initial arguments
    array: function array(i) {
        var s = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
        return Array.prototype.slice.call(i, s);
    },


    // capitalize first letter of string
    cap: function cap(s) {
        return s.slice(0, 1).toUpperCase() + s.slice(1);
    },


    // Add class name prefix to `string`
    cnp: function cnp(s) {
        var _this2 = this;

        return s.split(/\s+/).map(function (n) {
            return _this2.vars.cnPrefix + n;
        }).join(' ');
    },


    // shallow object copy
    copy: function copy(o) {
        return Object.assign({}, o);
    },


    // create a new element using tag anme and attributes
    create: function create(t) {
        var a = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return this.addAttrs(this.vars.document.createElement(t), a);
    },


    // convert dashed string to camel case
    dashcam: function dashcam(s) {
        var _this3 = this;

        return s.split(/-/).map(function (e, i) {
            return i > 0 ? _this3.cap(e) : e;
        }).join('');
    },


    // remove styles from element
    remStyle: function remStyle(e) {
        var _this4 = this;

        var a = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
        a.forEach(function (s) {
            return e.style[_this4.vars.fnRemove](s);
        });
    },


    // find first element in document or given element by selector
    find: function find(s, e) {
        return (e || this.vars.document).querySelector(s);
    },


    // find all elements in document or given element by selector
    findAll: function findAll(s, e) {
        return (e || this.vars.document).querySelectorAll(s);
    },


    // get computed style value, clearing inline styles first if requested
    comStyle: function comStyle(e, n) {
        var c = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
        if (c) this.remStyle(e, [n]);return this.styleObj(e)[n];
    },


    // calculate hypotenuse
    hypo: function hypo(x, y) {
        return Math.sqrt(x * x + y * y);
    },


    // convert number to milliseconds (multiply by 1000)
    ms: function ms(n) {
        return n * 1000;
    },


    // returns new object with processed keys and values
    process: function process(o, p, v) {
        return Object.keys(o).reduce(function (a, k) {
            a[p(k)] = v(o[k]);return a;
        }, {});
    },


    // append 'px' to numeric values
    px: function px(v) {
        return typeof v === 'number' ? v + 'px' : (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === 'object' ? this.pxo(v) : v;
    },


    // append 'px' to numeric values in object
    pxo: function pxo(o) {
        if ((typeof o === 'undefined' ? 'undefined' : _typeof(o)) === 'object') {
            for (var v in o) {
                o[v] = this.px(o[v]);
            }return o;
        } else return this.px(o);
    },


    // random number from 0 to given number (exclusive)
    random: function random(n) {
        return Math.floor(Math.random() * n);
    },


    // remove all selected node children of element
    remAll: function remAll(s, e) {
        return this.array(this.findAll(s, e)).map(function (n) {
            return n.parentNode.removeChild(n);
        });
    },


    // set attributes on element
    setAttr: function setAttr(e) {
        var o = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        Object.keys(o).forEach(function (k) {
            return e[k] = o[k];
        });
    },


    // add styles to element
    setStyle: function setStyle(e) {
        var o = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        Object.keys(o).forEach(function (k) {
            return e.style[k] = o[k];
        });
    },


    // shuffle array elements (in place)
    shuffle: function shuffle(a) {
        for (var i = a.length; i; i--) {
            this.swap(a, i - 1, this.random(i - 1));
        }return a;
    },


    // get style object for element
    styleObj: function styleObj(e) {
        return this.vars.window.getComputedStyle(e);
    },


    // swap values in array at given indices
    swap: function swap(a, x, y) {
        var _ref = [a[y], a[x]];
        a[x] = _ref[0];
        a[y] = _ref[1];
        return a;
    },


    // generate a unique id
    uid: function uid() {
        return (this.vars.inc++).toString(16).toUpperCase();
    },


    // keys of object in value order
    valueSort: function valueSort(o) {
        return Object.keys(o).map(function (k) {
            return [k, o[k]];
        }).sort(function (a, b) {
            return a[1] - b[1];
        }).map(function (i) {
            return i[0];
        });
    },


    vars: {
        cnPrefix: 'ts-',
        document: browser.document,
        inc: Math.ceil(Math.random() * 100000),
        fnRemove: browser.document.documentElement.style.removeProperty ? 'removeProperty' : 'removeAttribute',
        window: browser.window
    }
};

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var ins = function ins(s, v) {
    return s.replace(/\[x\]/g, v);
};
var sos = function sos(s) {
    return s.split(/\s+/);
};

var x = exports;

x.events = sos('rotate toggle');
x.boxmod = sos('margin-[x] border-[x]-width padding-[x]');
x.citem = sos('slide thumb');
x.view = sos('hidden visible transient overlay');
x.mutate = sos('add remove toggle');
x.plane = sos('vertical horizontal');
x.scaleType = sos('content-max content-min viewer none');

x.dimpos = { width: ['left', 'right'], height: ['top', 'bottom'] };
x.corpos = { origin: ['top', 'left'], extent: ['bottom', 'right'] };
// aliasing
x.alias = {
    width: 'horizontal', height: 'vertical', horizontal: 'width', vertical: 'height',
    prev: 'origin', next: 'extent'
};

// calculated keys
x.dim = Object.keys(x.dimpos);
x.cor = Object.keys(x.corpos);

x.pos = Object.keys(x.corpos).reduce(function (a, k) {
    return a.concat(x.corpos[k]);
}, []);

x.posdim = x.dim.reduce(function (a, d) {
    x.dimpos[d].forEach(function (p) {
        return a[p] = d;
    });return a;
}, {});
x.poscor = x.cor.reduce(function (a, d) {
    x.corpos[d].forEach(function (p) {
        return a[p] = d;
    });return a;
}, {});

x.comp = x.cor.reduce(function (a, c) {
    var v = x.corpos[c];var _ref = [v[1], v[0]];
    a[v[0]] = _ref[0];
    a[v[1]] = _ref[1];
    return a;
}, {});

x.box = x.boxmod.reduce(function (a, m) {
    x.pos.forEach(function (p) {
        return a.push(ins(m, p));
    });return a;
}, []);

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var u = __webpack_require__(0);

/*
    JQuery tweener requires JQuery library.
 */
module.exports.jquery = function (node, time, params) {
    var options = {
        complete: params.after,
        duration: time * 1000,
        start: params.before,
        progress: params.during,
        easing: params.ease,
        queue: false
    };

    $(node).animate(Object.assign({}, params.attrs, params.styles), options);
};

/*
    GSAP tweener requires TweenMax and ScrollToPlugin.
 */
module.exports.gsap = function (node, time, params) {
    // istanbul apparently cannot handle a destructure/rename/default scenario
    // var { styles: args = {}, attrs = {} } = params;
    // reworking above line of code...
    var _params$styles = params.styles,
        styles = _params$styles === undefined ? {} : _params$styles,
        _params$attrs = params.attrs,
        attrs = _params$attrs === undefined ? {} : _params$attrs,
        args = styles;


    var scrollTo = function scrollTo() {
        return args.scrollTo = args.scrollTo || {};
    };

    args.onStart = params.before;
    args.onComplete = params.after;
    args.onUpdate = params.during;
    args.ease = params.ease;

    if (typeof attrs.scrollLeft === 'number') scrollTo().x = attrs.scrollLeft;
    if (typeof attrs.scrollTop === 'number') scrollTo().y = attrs.scrollTop;

    TweenLite.to(node, time, args);
};

/*
    Default non-tween function
 */
module.exports.none = function (node, time, params) {
    if (params.before) params.before();
    u.setAttr(node, params.attrs || {});
    u.setStyle(node, params.styles || {});
    if (params.during) params.during();
    if (params.after) params.after();
};

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var keys = __webpack_require__(1);
var tweeners = __webpack_require__(2);
var transitions = __webpack_require__(4);

exports.defaults = {
    // classname to add to active slide/thumb in carousel
    activeClass: 'active',
    // specify function for animation (function(node, time, params)
    animator: tweeners.none,
    // icon classnames for controls
    icons: null,
    // pause rotation on mouseovers?
    pauseOnMouseover: true,
    // setup custom responsive dimension thresholds
    responsive: { xs: 480, sm: 768, md: 1024, lg: 1200 },
    // seconds of rotation animation
    rotateDuration: 0.8,
    // easing value for rotate animation (library specific)
    rotateEasing: null,
    // seconds per rotation
    rotateInterval: 3,
    // seconds to wait before rotation restart after some interaction
    rotateIntermission: 3,
    // number of items to traverse per rotation
    rotateStep: 1,
    // selector display options ('hidden', 'visible', 'transient', 'overlay')
    selectorDisplay: 'visible',
    // seconds to wait before hiding selector ('transient' only)
    selectorHideDelay: 0,
    // position selector at 'top', 'bottom', 'left', or 'right'
    selectorPosition: 'bottom',
    // size (height or width) of selector as percentage of container
    selectorSize: 0.20,
    // show control icons on mouseover
    showIconsOnHover: true,
    // show selector initially when 'transient'?
    showSelector: true,
    // show slide content?
    showSlideContent: true,
    // show thumbnail content?
    showThumbContent: true,
    // shuffle carousel content items
    shuffle: false,
    // slide scaling style
    slideScaleType: { img: 'content-max' },
    // slide transition type
    slideTransition: transitions.slide,
    // element name to open carousel with
    startAt: null,
    // thumb scaling style
    thumbScaleType: { img: 'content-max' },
    // a ratio of secondary over primary dimension for the thumbnail
    thumbSize: 1.0,
    // seconds of selector hide/show animation
    toggleDuration: 0.5,
    // easing value for toggle animation (library specific)
    toggleEasing: null,
    // size (height or width) of toggle control as percentage of selector
    toggleSize: 0.10,
    // viewer display options ('hidden', 'visible')
    viewerDisplay: 'visible',
    // viewer slides scroll in 'vertical' or 'horizontal' direction
    viewerOrientation: 'horizontal'
};
// default all directional icons to 'deficon'
exports.objectDefaults = {
    icons: keys.pos.reduce(function (a, p) {
        a[p] = 'deficon';return a;
    }, {}),
    slideScaleType: { def: 'none' },
    thumbScaleType: { def: 'viewer' }
};
exports.enums = {
    selectorDisplay: keys.view,
    selectorPosition: keys.pos,
    slideScaleType: keys.scaleType,
    thumbScaleType: keys.scaleType,
    viewerDisplay: keys.view.slice(0, 2),
    viewerOrientation: keys.plane
};

var validation = {
    message: function message(name, value, type) {
        var message = 'Invalid value (' + value + ') specified for config option "' + name + '". ';

        if (exports.enums[name]) message = message + ('Option must be one of ' + exports.enums[name].join(', ') + '.');else message = message + ('Option must be of ' + type + ' type.');

        return message;
    },

    funcs: {
        boolean: function boolean(v) {
            return (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === _typeof(true);
        },
        fixed: function fixed(v, n) {
            return exports.enums[n].indexOf(v) >= 0;
        },
        function: function _function(v) {
            return (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === _typeof(function () {});
        },
        number: function number(v) {
            return (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === _typeof(0);
        },
        string: function string(v) {
            return (typeof v === 'undefined' ? 'undefined' : _typeof(v)) === _typeof('');
        }
    },

    types: {
        boolean: 'pauseOnMouseover showSelector showSlideContent showThumbContent shuffle',
        fixed: 'selectorDisplay selectorPosition slideScaleType thumbScaleType viewerOrientation',
        function: 'animator slideTransition',
        number: 'responsive rotateDuration rotateInterval rotateIntermission rotateStep selectorSize thumbSize\n            toggleDuration',
        string: 'activeClass'
    },

    exec: function exec(key, value, type) {
        if (value == null || (typeof value === 'undefined' ? 'undefined' : _typeof(value)) !== 'object') {
            if (!this.funcs[type](value, key)) throw new Error(this.message(key, value, type));
        } else {
            for (var k in value) {
                this.exec(key, value[k], type);
            }
        }
    },

    validate: function validate(options) {
        var _this = this;

        var types = this.types;


        Object.keys(types).forEach(function (t) {
            return types[t].split(/\s+/).forEach(function (n) {
                return _this.exec(n, options[n], t);
            });
        });

        return options;
    }
};

exports.check = validation.validate.bind(validation);

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var u = __webpack_require__(0);

exports.fade = function (anim, dims, plane, forward) {
    var prev = function prev(node) {
        anim.events().node(node);

        anim.after(function () {
            u.setStyle(node, { visibility: 'hidden' });
            u.remStyle(node, ['opacity']);
        });

        anim.toStyle({ opacity: 0 });
    };

    var next = function next(node) {
        anim.events().node(node);

        u.setStyle(node, { opacity: 0, visibility: 'visible' });

        anim.after(function () {
            u.remStyle(node, ['opacity']);
        });

        anim.toStyle({ opacity: 1 });
    };

    return { prev: prev, next: next };
};

exports.slide = function (anim, dims, plane, forward) {
    var prev = exports.unstack(anim, dims, plane, forward).prev;
    var next = exports.stack(anim, dims, plane, forward).next;

    return { prev: prev, next: next };
};

exports.stack = function (anim, dims, plane, forward) {
    var prev = function prev(node) {
        anim.events().node(node);

        anim.after(function () {
            u.setStyle(node, { visibility: 'hidden' });
        });
        // only tweening here to prevent immediate node disappearance
        anim.toStyle({ top: 0, left: 0 });
    };

    var next = function next(node) {
        anim.events().node(node);

        u.setStyle(node, u.px({
            top: plane === 'horizontal' ? 0 : forward ? dims.height : -dims.height,
            left: plane === 'vertical' ? 0 : forward ? dims.width : -dims.width,
            visibility: 'visible',
            zIndex: '1'
        }));

        anim.after(function () {
            u.remStyle(node, ['z-index']);
        });

        anim.toStyle(u.px({ top: 0, left: 0 }));
    };

    return { prev: prev, next: next };
};

exports.unstack = function (anim, dims, plane, forward) {
    var prev = function prev(node) {
        anim.events().node(node);

        u.setStyle(node, { zIndex: '1' });

        anim.after(function () {
            u.setStyle(node, u.px({ top: 0, left: 0, visibility: 'hidden' }));
            u.remStyle(node, ['z-index']);
        });

        anim.toStyle(u.px({
            top: plane === 'horizontal' ? 0 : forward ? -dims.height : dims.height,
            left: plane === 'vertical' ? 0 : forward ? -dims.width : dims.width
        }));
    };

    var next = function next(node) {
        u.setStyle(node, { visibility: 'visible' });
    };

    return { prev: prev, next: next };
};

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var Carousel = __webpack_require__(10);
var options = __webpack_require__(3);
var u = __webpack_require__(0);

var carousels = {},
    configs = {};
// options merge and check
var check = function check(opts, defs) {
    return options.check(Object.assign({}, defs || options.defaults, opts));
};

module.exports = {
    create: function create(id, opts) {
        var elem = u.find('#' + id);

        return elem ? carousels[id] = new Carousel(elem, configs[id] = check(opts)).interface : null;
    },
    get: function get(id) {
        return carousels[id];
    },
    names: function names() {
        return Object.keys(carousels);
    },
    options: function options(id) {
        return Object.assign({}, configs[id]);
    },
    update: function update(id, opts) {
        return carousels[id].reset(configs[id] = check(opts, configs[id]));
    },


    tweeners: __webpack_require__(2),

    transitions: __webpack_require__(4)
};

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


try {
    var _require = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"mock-browser\""); e.code = 'MODULE_NOT_FOUND';; throw e; }())),
        MockBrowser = _require.mocks.MockBrowser;

    exports.document = MockBrowser.createDocument();
    exports.window = MockBrowser.createWindow();
} catch (e) {
    exports.document = document;
    exports.window = window;
}

/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var tweeners = __webpack_require__(2);
var u = __webpack_require__(0);

module.exports = function () {
    var args = {},
        params = {};
    // get arguments (up to 3 parameters)
    for (var i = 0, imx = Math.min(arguments.length, 3); i < imx; i++) {
        var argument = arguments[i];

        switch (typeof argument === 'undefined' ? 'undefined' : _typeof(argument)) {
            case 'function':
                args.exec = argument;break;
            case 'string':
                params.name = argument;break;
            case 'number':
                args.time = argument;break;
            case 'boolean':
                args.jump = argument;break;
            case 'object':
                var _ref = [argument.args, argument.params];
                var _ref$ = _ref[0];
                args = _ref$ === undefined ? argument : _ref$;
                var _ref$2 = _ref[1];
                params = _ref$2 === undefined ? {} : _ref$2;
                break;
        }
    }

    var methods = {
        // function to run after animation completes
        after: function after(f) {
            params.after = f;return this;
        },

        // function to run before animation starts
        before: function before(f) {
            params.before = f;return this;
        },

        // clone this animator
        clone: function clone() {
            return module.exports({ args: u.copy(args), params: u.copy(params) });
        },

        // function to for each animation frame
        during: function during(f) {
            params.during = f;return this;
        },

        // easing value for animation
        ease: function ease(e) {
            params.ease = e;return this;
        },

        // set/reset all event functions (before, after, during)
        events: function events(b, a, d) {
            this.before(b);this.after(a);this.during(d);return this;
        },

        // the function to execute animation
        exec: function exec(f) {
            args.exec = f;return this;
        },

        // apply jump setting
        jump: function jump(b) {
            args.jump = b;return this;
        },

        // name for this animator
        name: function name(n) {
            params.name = n;return this;
        },

        // the element to be animated
        node: function node(n) {
            args.node = n;return this;
        },

        // animation duration
        time: function time(t) {
            args.time = t;return this;
        },

        // tween attributes
        toAttr: function toAttr(attrs, jump) {
            this.to({ attrs: attrs }, jump);
        },

        // tween css styles
        toStyle: function toStyle(styles, jump) {
            this.to({ styles: styles }, jump);
        },

        // run animation
        to: function to(items, jump) {
            (jump || args.jump || typeof args.exec !== 'function' ? tweeners.none : args.exec)(args.node, args.time, Object.assign({}, params, items));
        }
    };

    return methods;
};

/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var u = __webpack_require__(0);

var names = ['carousel', 'viewer', 'selector', 'view', 'select', 'slides', 'thumbs', 'prev', 'next', 'focus', 'toggle'];

var icons = ['prev', 'next'];

module.exports = function (nodes) {
    var methods = {
        appendContent: function appendContent(content, shuffle) {
            u.remAll('.' + u.cnp('slide'), nodes.slides);
            u.remAll('.' + u.cnp('thumb'), nodes.thumbs);
            // shuffle the content necessary
            if (shuffle) u.shuffle(content.order);
            // add all slides/thumbs to containers
            content.order.forEach(function (name) {
                nodes.slides.appendChild(content.slides[name]);
                nodes.thumbs.appendChild(content.thumbs[name]);
            });

            return this;
        },
        assemble: function assemble(root) {
            // create all structural nodes
            names.forEach(function (n) {
                return nodes[n] = u.create('div', { class: (icons.indexOf(n) >= 0 ? 'control ' : '') + n });
            });
            // assembly node structure (arranged below to show hierarchy)
            nodes.carousel.appendChild(nodes.viewer).appendChild(nodes.prev).parentNode.appendChild(nodes.view).appendChild(nodes.slides).parentNode.parentNode.appendChild(nodes.next).parentNode.parentNode.appendChild(nodes.selector).appendChild(nodes.select).appendChild(nodes.thumbs).appendChild(nodes.focus).parentNode.parentNode.parentNode.appendChild(nodes.toggle);
            // create and append all icon nodes
            icons.forEach(function (n) {
                return nodes[n + 'Icon'] = nodes[n].appendChild(u.create('i'));
            });

            if (root) {
                u.remAll('*', root);
                root.appendChild(nodes.carousel);
                nodes.root = root;
            }

            return this;
        }
    };

    return methods;
};

/***/ }),
/* 9 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var u = __webpack_require__(0);

module.exports = function () {
    var methods = {
        slides: {},
        thumbs: {},
        order: [],

        /**
            Determines the element to use as the thumbnail from the
            given `slide`.
             This is resolved by looking for an element in the slide with a
            'data-tslide-thumb' attribute.  If not found, the first non-text
            child found in the slide is used.
             By default, the element found will be fully cloned to avoid it
            being removed from the slide.  To remove the element from the
            slide (prevent cloning) specify 'remove' in the 'data-tsilde-thumb'
            attribute.
         */
        getThumbnail: function getThumbnail(slide) {
            var thumbAttr = u.anp('thumb');
            var thumb = u.find('[' + thumbAttr + ']', slide) || u.find('*', slide) || slide;

            return thumb === slide || thumb.getAttribute(thumbAttr) !== 'remove' ? thumb.cloneNode(true) : thumb;
        },
        index: function index(cnode) {
            var _this = this;

            u.array(cnode.childNodes).forEach(function (node) {
                if (node.getAttribute) _this.insert(node, node.getAttribute(u.anp('name')));
            });

            return this;
        },
        insert: function insert(slide, title, index) {
            var name = title || u.uid();

            this.slides[name] = this.toContentItem(slide, 'slide', name);
            this.thumbs[name] = this.toContentItem(this.getThumbnail(slide), 'thumb', name);
            // remove from order if existing
            var atIndex = this.order.indexOf(name);
            if (atIndex >= 0) this.order.splice(atIndex, 1);
            // add to order at requested index or at end of order
            var insertAt = typeof index !== 'number' || index > this.order.length ? this.order.length : index;
            this.order.splice(insertAt, 0, name);

            return this;
        },
        toContentItem: function toContentItem(elem, type, name) {
            // slide or thumb item
            var item = u.create('div', _defineProperty({ class: type }, u.anp('name'), name));
            // overlay layer for thumb only
            var overlay = type === 'thumb' ? item.appendChild(u.create('div', { class: type + '-overlay' })) : item;
            // content container
            var liner = overlay.appendChild(u.create('div', { class: type + '-content' }));
            // add content element to container
            liner.appendChild(elem);

            return item;
        }
    };

    return methods;
};

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var eventMixer = __webpack_require__(14);

var animator = __webpack_require__(7);
var keys = __webpack_require__(1);
var timer = __webpack_require__(13);
var u = __webpack_require__(0);

var assembler = __webpack_require__(8);
var contentHandler = __webpack_require__(9);
var nodecs = __webpack_require__(11);
var responsive = __webpack_require__(12);

var Carousel = function Carousel() {
    this.init.apply(this, arguments);
};
Carousel.prototype = {
    init: function init(elem, options) {
        var _this = this;

        this.root = elem;
        // create timers functionality
        this.timers = this.createTimers();
        // events we are listening to
        this.listeners = this.createEventListeners();
        // create an interface for this carousel
        this.interface = this.createInterface();
        // event manager
        this.em = keys.events.reduce(function (m, n) {
            m.create(n);return m;
        }, eventMixer.mixer(this.interface));
        // events we will fire ourselves
        this.events = this.em.names().reduce(function (a, n) {
            a[n] = _this.em.event(n);return a;
        }, {});
        // index all content from root node
        this.content = contentHandler().index(this.root);
        // assemble the carousel for all structural nodes
        this.assembler = assembler(this.nodes = {}).assemble(this.root);
        // add event listeners
        this.listen(this.listeners, this.nodes);
        // continue initialization via .reset()
        this.reset(options);
    },
    createAnimators: function createAnimators(ro) {
        var animators = {},
            executor = ro.animator();

        animators.rotate = animator('carouselRotate', executor, ro.rotateDuration()).ease(ro.rotateEasing());
        animators.toggle = animator('selectorToggle', executor, ro.toggleDuration()).ease(ro.toggleEasing());

        return animators;
    },
    createEventListeners: function createEventListeners() {
        var _this2 = this;

        var listeners = {
            carousel: {
                mouseEnter: function mouseEnter(e) {
                    if (_this2.ro.pauseOnMouseover()) _this2.state.hovered = true;
                },
                mouseLeave: function mouseLeave(e) {
                    if (_this2.ro.pauseOnMouseover()) {
                        _this2.state.hovered = false;_this2.pauseRotation();
                    }
                }
            },
            next: {
                click: function click(e) {
                    _this2.pauseRotation();_this2.moveCurrentIndex(1);
                }
            },
            prev: {
                click: function click(e) {
                    _this2.pauseRotation();_this2.moveCurrentIndex(-1);
                }
            },
            select: {
                mouseOver: function mouseOver(e) {
                    u.setStyle(_this2.nodes.select, { overflow: 'auto' });_this2.state.syncSelector = false;
                },
                mouseLeave: function mouseLeave(e) {
                    u.remStyle(_this2.nodes.select, ['overflow']);_this2.state.syncSelector = true;
                }
            },
            selector: {
                mouseOver: function mouseOver(e) {
                    return _this2.timers.selectorHide.clear();
                },
                mouseLeave: function mouseLeave(e) {
                    return _this2.toggleHideDelay();
                }
            },
            thumbs: {
                click: function click(e) {
                    return _this2.onThumbClick(e, e.target);
                }
            },
            toggle: {
                click: function click(e) {
                    return _this2.toggleSelector();
                }
            },
            window: {
                load: function load(e) {
                    _this2.remodel();_this2.setCurrentIndex(undefined, true);
                },
                resize: function resize(e) {
                    return _this2.timers.eventRemodel.start(true);
                }
            }
        };

        return listeners;
    },
    createInterface: function createInterface() {
        var _this3 = this;

        var face = {
            currentIndex: function currentIndex() {
                return _this3.state.current.index;
            },
            getName: function getName() {
                return _this3.root.getAttribute('id');
            },
            getSlide: function getSlide(i) {
                return _this3.itemAt('slide', i);
            },
            getThumb: function getThumb(i) {
                return _this3.itemAt('thumb', i);
            },
            itemCount: function itemCount() {
                return _this3.content.order.length;
            },
            remodel: function remodel() {
                _this3.remodel();return face;
            },
            start: function start() {
                _this3.startRotation();return face;
            },
            stop: function stop() {
                _this3.stopRotation();return face;
            }
        };

        return face;
    },
    createTimers: function createTimers() {
        var _this4 = this;

        var timers = {
            eventRemodel: timer(function () {
                return _this4.remodel();
            }, 0),
            resumeRotation: timer(function () {
                return _this4.state.paused = false;
            }),
            rotate: timer(function () {
                if (!_this4.isSuspended()) _this4.setCurrentIndex(_this4.nextIndex());
            }, true),
            selectorHide: timer(function () {
                return _this4.toggleSelector(false);
            })
        };

        return timers;
    },
    isSuspended: function isSuspended() {
        return this.state.paused || this.ro.pauseOnMouseover() && this.state.hovered;
    },
    itemAt: function itemAt(type, id) {
        var index = typeof id === 'number' ? id : this.content.order.indexOf(id);
        var valid = index >= 0 && index < this.content.order.length;

        return valid ? this.nodes[type + 's'].childNodes[index + (type === 'thumb' ? 1 : 0)] : null;
    },
    listen: function listen(listeners, nodes) {
        var add = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

        var action = (add ? 'add' : 'remove') + 'EventListener';

        for (var listenKey in listeners) {
            var node = nodes[listenKey] || u.vars[listenKey];

            for (var eventKey in listeners[listenKey]) {
                node[action](eventKey.toLowerCase(), listeners[listenKey][eventKey]);
            }
        }
    },


    /*
        Move carousel `count` number of items from the current index.
         If resulting index is less than 0 then first item is selected.
        If resulting index is more than item count then last item is selected.
      */
    moveCurrentIndex: function moveCurrentIndex(count) {
        var newIndex = this.state.current.index + count;

        if (newIndex < 0) newIndex = 0;else if (newIndex >= this.content.order.length) newIndex = this.content.order.length - 1;

        if (this.state.current.index !== newIndex) this.setCurrentIndex(newIndex);
    },
    nextIndex: function nextIndex() {
        var newIndex = this.state.current.index + this.state.rotateStep;
        // first and last indexes
        var findex = 0,
            lindex = this.content.order.length - 1;

        if (newIndex < findex) {
            this.state.rotateStep *= -1;
            newIndex = findex - newIndex;
        } else if (newIndex > lindex) {
            this.state.rotateStep *= -1;
            newIndex = lindex - (newIndex - lindex);
        }

        return newIndex;
    },
    onThumbClick: function onThumbClick(e, target) {
        var name = target.getAttribute(u.anp('name'));

        if (name) {
            this.pauseRotation();
            this.setCurrentIndex(this.content.order.indexOf(name), false, true);
        } else if (e.currentTarget !== target) {
            this.onThumbClick(e, target.parentElement);
        }
    },


    /*
        Pauses auto-rotation for the time specified in config options.
     */
    pauseRotation: function pauseRotation() {
        this.state.paused = true;
        this.timers.resumeRotation.wait(u.ms(this.ro.rotateIntermission())).start(true);
    },
    remodel: function remodel() {
        this.calcs.base.remodel();
    },


    /**
        Fully reset this carousel with new config options.
     */
    reset: function reset(options) {
        var _this5 = this;

        // clear all timers
        Object.keys(this.timers).forEach(function (t) {
            return _this5.timers[t].clear();
        });
        // setup responsive options
        this.ro = responsive(options, this.root);
        // setup some initial state
        this.state = {
            content: this.content,
            current: {},
            isSelectorShown: this.ro.selectorIsShown(),
            previous: {},
            rotateStep: this.ro.rotateStep(),
            syncSelector: true
        };
        // add content nodes to carousel
        this.assembler.appendContent(this.content, this.ro.shuffle());
        // for various calculations
        this.calcs = nodecs(this.nodes, this.ro, this.state);
        // create animators functionality
        this.animators = this.createAnimators(this.ro);
        // set the initial content item to be displayed
        this.state.current.index = this.calcs.base.startIndex();

        this.toggleHideDelay();
    },


    /*
        Makes `index` current in the carousel.
     */
    setCurrentIndex: function setCurrentIndex() {
        var index = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : this.state.current.index;

        var _this6 = this;

        var jump = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
        var sync = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

        if (index >= 0 && index < this.content.order.length && this.events.rotate.fire({ nextIndex: index })) {
            var rotate = this.animators.rotate,
                _calcs = this.calcs,
                focus = _calcs.focus,
                select = _calcs.select,
                slide = _calcs.slide,
                nodes = this.nodes,
                state = this.state;
            var current = state.current,
                previous = state.previous,
                citem = keys.citem;
            // set the new index

            previous.index = current.index || 0;
            current.index = index;
            // make sure selector is showing current thumb
            state.firstThumbIndex = select.firstThumb(current.index);
            // update previous/current content items
            citem.forEach(function (c) {
                previous[c] = current[c];current[c] = _this6.itemAt(c, current.index);
            });
            // content item active class
            var activeClass = this.ro.activeClass();
            // tween the focus element
            rotate.jump(jump).node(nodes.focus).after(function () {
                return citem.forEach(function (c) {
                    if (current[c]) current[c].classList.add(activeClass);
                });
            }).before(function () {
                return citem.forEach(function (c) {
                    if (previous[c]) previous[c].classList.remove(activeClass);
                });
            }).toStyle(u.px(focus.origin()));
            // scroll selector conditionally
            if (state.syncSelector || sync) rotate.events().node(nodes.select).toAttr(select.scrollPositions());
            // do slide transition
            this.slideTransition();
        }
    },
    slideTransition: function slideTransition() {
        var animators = this.animators,
            calcs = this.calcs,
            state = this.state,
            _state = this.state,
            current = _state.current,
            previous = _state.previous;
        // get transition method and viewer orientation

        var transition = this.ro.slideTransition(),
            orientation = this.ro.viewerOrientation();
        // determine relative direction of transition
        var forward = current.index > previous.index || current.index === previous.index && state.rotateStep > 0;
        // get transition functions for previous/next slides

        var _transition = transition(animators.rotate.clone(), calcs.slide.dims(), orientation, forward),
            prev = _transition.prev,
            next = _transition.next;
        // transition the new current slide


        if (next && current.slide) next(current.slide);
        // transition previous slide only if different from current
        if (prev && previous.slide && current.index !== previous.index) prev(previous.slide);
    },


    /*
        Initiates auto-rotation if rotation interval > 0.
     */
    startRotation: function startRotation() {
        if (this.content.order.length > 0 && this.calcs.base.isAutoRotated()) {
            this.timers.rotate.wait(u.ms(this.ro.rotateInterval())).start();
        }
    },


    /*
        Stops auto-rotation.
     */
    stopRotation: function stopRotation() {
        this.timers.rotate.clear();
    },
    toggleHideDelay: function toggleHideDelay() {
        if (this.ro.isSelectorDisplayTransient()) {
            var hideDelay = this.ro.selectorHideDelay();

            if (hideDelay > 0) this.timers.selectorHide.wait(u.ms(hideDelay)).start(true);
        }
    },
    toggleSelector: function toggleSelector(display) {
        if (this.ro.isSelectorDisplayTransient()) {
            var animators = this.animators,
                _calcs2 = this.calcs,
                selector = _calcs2.selector,
                prev = _calcs2.prev,
                next = _calcs2.next,
                nodes = this.nodes,
                ro = this.ro,
                state = this.state;
            // if `display` is not boolean then ignore

            var show = typeof display === 'boolean' ? display : !state.isSelectorShown;

            if (this.events.toggle.fire({ shown: state.isSelectorShown, willShow: show })) {
                // clear any pending timer
                this.timers.selectorHide.clear();
                // prepare to hide selector if now being shown
                if (show) this.toggleHideDelay();
                // update selector display state
                state.isSelectorShown = show;
                // perform show/hide animation
                animators.toggle.node(nodes.selector).toStyle(u.px(_defineProperty({}, selector.position(), selector.togglePos())));
            }
        }
    }
};

module.exports = Carousel;

/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var u = __webpack_require__(0);
var keys = __webpack_require__(1);

module.exports = function (nodes, ro, state) {
    var order = state.content.order,
        calcs = {};
    // base node calculator functionality
    var basec = function basec() {
        var nodec = { vars: {}, last: {}, calc: {} },
            mCache = {};
        // clear calculator cache
        nodec.clear = function () {
            nodec.last = nodec.vars;nodec.vars = {};
        };
        // extend the calculator
        nodec.extend = function (node, parentc) {
            var typeDim = function typeDim(d, t) {
                var e = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : '';
                return nodec[t + u.cap(keys.dimpos[d][0]) + e]() + nodec[t + u.cap(keys.dimpos[d][1]) + e]();
            };

            nodec.node = node;nodec.cl = node.classList;
            // individual functions for margins, border-width, and padding
            keys.box.forEach(function (k) {
                return mCache[u.dashcam(k)] = function () {
                    return parseFloat(u.comStyle(nodec.node, k, true));
                };
            });
            // default top, left, bottom, right functions
            keys.pos.forEach(function (k) {
                return mCache[k] = function () {
                    return parentc ? parentc['padding' + u.cap(k)]() : 'auto';
                };
            });

            keys.dim.forEach(function (d) {
                var dc = u.cap(d);

                mCache['margins' + dc] = function () {
                    return typeDim(d, 'margin');
                };
                mCache['borders' + dc] = function () {
                    return typeDim(d, 'border', 'Width');
                };
                mCache['padding' + dc] = function () {
                    return typeDim(d, 'padding');
                };
                mCache['padders' + dc] = function () {
                    return nodec['padding' + dc]() + nodec['borders' + dc]();
                };
                // border box and full dims; one must be overriden if no parent!
                mCache['border' + dc] = function () {
                    return nodec[d]() - nodec['margins' + dc]();
                };
                if (parentc) mCache[d] = function () {
                    return parentc['content' + dc]();
                };else mCache[d] = function () {
                    return nodec['border' + dc]() + nodec['margins' + dc]();
                };
                // content box dimensions
                mCache['content' + dc] = function () {
                    return nodec['border' + dc]() - nodec['padders' + dc]();
                };
            });

            nodec.dims = function (t) {
                return keys.dim.reduce(function (a, d) {
                    a[d] = nodec[t ? t + u.cap(d) : d]();return a;
                }, {});
            };

            nodec.object = function (l) {
                return l.reduce(function (a, p) {
                    a[p] = nodec[p]();return a;
                }, {});
            };

            nodec.origin = function () {
                return nodec.object(keys.corpos.origin);
            };

            nodec.pos = function () {
                return nodec.object(keys.pos);
            };

            keys.mutate.forEach(function (k) {
                return nodec[k + 'Class'] = function () {
                    nodec.cl[k].apply(nodec.cl, arguments);
                };
            });

            nodec.updateClass = function (n) {
                nodec.removeClass(nodec.last[n]);nodec.addClass(nodec[n]());
            };

            nodec.style = function () {
                u.setStyle(nodec.node, u.px(Object.assign.apply(u, arguments)));
            };

            nodec.attrib = function () {
                u.setAttr(nodec.node, Object.assign.apply(u, arguments));
            };

            nodec.hypotenuse = function () {
                return u.hypo(nodec.contentWidth(), nodec.contentHeight());
            };

            return nodec;
        };

        nodec.cacheExtend = function () {
            // create caching methods
            Object.keys(Object.assign(mCache, nodec.calc)).forEach(function (k) {
                nodec[k] = nodec[k] || function () {
                    return nodec.vars[k] = nodec.vars[k] || mCache[k]();
                };
            });

            return nodec;
        };

        return nodec;
    };

    var fnCalcs = {};

    /*
        Base carousel information calculator
     */
    fnCalcs.base = function (fn) {
        var calc = fn.calc;
        // is carousel auto rotated?

        calc.isAutoRotated = function () {
            return ro.rotateInterval() >= 0 && ro.rotateInterval() >= ro.rotateDuration();
        };
        // orientation of selector ('horizontal' or 'vertical')
        calc.selectorOrientation = function () {
            return keys.alias[keys.posdim[keys.comp[ro.selectorPosition()]]];
        };
        // carousel starting index
        calc.startIndex = function () {
            var startAt = ro.startAt();
            var index = typeof startAt === 'number' ? startAt : order.indexOf(startAt);
            if (index < 0) index = order.length + index;
            return index >= 0 && index < order.length ? index : 0;
        };
        // full carousel cache reset and remodel
        fn.remodel = function () {
            Object.keys(fnCalcs).forEach(function (n) {
                return gnc(n).clear();
            });
            gnc('carousel').remodel();
        };

        return fn;
    };

    /*
        Carousel node calculator
     */
    fnCalcs.carousel = function (fn) {
        var calc = fn.calc;
        // width, height methods

        keys.dim.forEach(function (d) {
            return calc['border' + u.cap(d)] = function () {
                return ro[d]();
            };
        });

        calc.widthSizeClass = function () {
            return u.cnp('width-' + ro.label('width'));
        };
        calc.heightSizeClass = function () {
            return u.cnp('height-' + ro.label('height'));
        };
        calc.selectorDisplayClass = function () {
            return u.cnp('selector-' + ro.selectorDisplay());
        };
        calc.selectorPositionClass = function () {
            return u.cnp('selector-' + ro.selectorPosition());
        };
        calc.selectorOrientationClass = function () {
            return u.cnp('selector-' + gnc('base').selectorOrientation());
        };
        calc.viewerDisplayClass = function () {
            return u.cnp('viewer-' + ro.viewerDisplay());
        };

        fn.remodel = function () {
            fn.updateClass('widthSizeClass');
            fn.updateClass('heightSizeClass');
            fn.updateClass('selectorDisplayClass');
            fn.updateClass('viewerDisplayClass');
            fn.toggleClass('hide-icons', ro.showIconsOnHover());

            if (!ro.isSelectorDisplayHidden()) {
                fn.updateClass('selectorPositionClass');
                fn.updateClass('selectorOrientationClass');
            }

            gnc('selector').remodel();
            gnc('viewer').remodel();

            fn.style(fn.dims('content'));
        };

        return fn.extend(nodes.carousel);
    };

    /*
        Selector focus node calculator
     */
    fnCalcs.focus = function (fn) {
        var calc = fn.calc;
        // top, left methods

        keys.corpos.origin.forEach(function (p) {
            return fn[p] = function () {
                return gnc('thumb')[p](state.current.index);
            };
        });
        // width, height methods
        keys.dim.forEach(function (d) {
            return calc[d] = function () {
                return gnc('thumb')[d]();
            };
        });

        fn.remodel = function () {
            return fn.style(fn.dims('content'), fn.origin());
        };

        return fn.extend(nodes.focus, gnc('thumb'));
    };

    /*
        Content item navigation node calculator
     */
    fnCalcs.next = fnCalcs.prev = function (fn, name) {
        var calc = fn.calc;


        var corner = keys.corpos[keys.alias[name]];
        // initial distance from edge of caontainer
        calc.initPos = function () {
            return parseFloat(u.comStyle(fn.node, fn.position(), true));
        };
        // position of this control
        calc.position = function () {
            return keys.dimpos[keys.alias[ro.viewerOrientation()]].find(function (p) {
                return corner.indexOf(p) >= 0;
            });
        };
        // is control position same as selector position?
        calc.withSelector = function () {
            return fn.position() === ro.selectorPosition();
        };
        // is a position adjustment needed?
        calc.posAdjustNeeded = function () {
            return ro.isSelectorDisplayTransient() && fn.withSelector();
        };
        // totoal offset value for selector and its toggle control
        calc.offset = function () {
            return gnc('selector').primary() + gnc('selector').controlDim();
        };

        fn.remodel = function () {
            var classList = fn.node.firstChild.classList;


            var dim = keys.alias[ro.viewerOrientation()],
                icons = ro.icons();
            // remove all classes from icon child
            fn.node.firstChild.setAttribute('class', '');
            // set correct css classes for this control
            corner.forEach(function (p) {
                var add = dim === keys.posdim[p];
                // set directional class name
                fn.toggleClass(u.cnp(p), add);
                // set icon class name(s)
                if (add) classList.add.apply(classList, icons[p].split(/\s+/));
            });

            // remove any prevously set position styles
            u.remStyle(fn.node, corner);
        };

        return fn.extend(nodes[name]);
    };

    /*
        Select node calculator
     */
    fnCalcs.select = function (fn) {
        var calc = fn.calc;
        // primary/secondary dimensions of selector ('width' or 'height')

        calc.primaryDim = function () {
            return keys.posdim[gnc('selector').position()];
        };
        calc.secondaryDim = function () {
            return keys.posdim[keys.comp[gnc('selector').position()]];
        };
        // number of thumbs in select view
        calc.viewCount = function () {
            return Math.round(fn[fn.secondaryDim()]() / (fn[fn.primaryDim()]() * ro.thumbSize()));
        };
        // middle thumbnail in view
        calc.middleThumb = function () {
            return Math.floor(fn.viewCount() / 2);
        };
        // index of the maximum first viewable thumb
        calc.maxFirstThumb = function () {
            return Math.max(order.length - fn.viewCount(), 0);
        };
        // offset of the first viewable thumb based on rotate direction (0 or 1)
        fn.firstOffset = function () {
            return fn.viewCount() % 2 == 0 && state.rotateStep < 0 ? 1 : 0;
        };
        // first viewable thumb for given item index
        fn.firstThumb = function (i) {
            return Math.min(Math.max(i - fn.middleThumb() + fn.firstOffset(), 0), fn.maxFirstThumb());
        };
        // scroll positions ('scrollTop' and 'scrollLeft')
        fn.scrollPositions = function () {
            return u.process(gnc('thumbs').pos(), function (n) {
                return 'scroll' + u.cap(n);
            }, Math.abs);
        };

        fn.remodel = function () {
            gnc('thumbs').remodel();
            gnc('focus').remodel();

            fn.style(fn.dims('content'), fn.origin());
            fn.attrib({}, fn.scrollPositions());
        };

        return fn.extend(nodes.select, gnc('selector'));
    };

    /*
        Selector node calculator
     */
    fnCalcs.selector = function (fn) {
        var calc = fn.calc;
        // cache selector position as its used a lot here

        calc.position = function () {
            return ro.selectorPosition();
        };
        // selector orientation dimension
        calc.planeDim = function () {
            return keys.posdim[keys.comp[fn.position()]];
        };

        keys.dim.forEach(function (d) {
            var is = 'is' + u.cap(keys.alias[d]),
                cnt = 'content' + u.cap(d);
            // isHorizontal, isVertical methods
            calc[is] = function () {
                return keys.dimpos[d].indexOf(fn.position()) < 0;
            };
            // width, height methods
            calc[d] = function () {
                return fn[is]() ? gnc('carousel')[cnt]() : Math.floor(ro.selectorSize() * gnc('carousel')[cnt]());
            };
        });

        keys.cor.forEach(function (d) {
            var is = 'is' + u.cap(d);
            // add diagonal boolean methods ('isOrigin', 'isExtent')
            calc[is] = function () {
                return keys.poscor[fn.position()] === d;
            };
            // add position methods ('top', 'left', 'bottom', 'right')
            keys.corpos[d].forEach(function (p) {
                return calc[p] = function () {
                    return !fn[is]() ? 'auto' : fn.position() === p ? fn.togglePos() : gnc('carousel')['padding' + u.cap(p)]();
                };
            });
        });
        // content padding at position of selector
        calc.basePadding = function () {
            return gnc('carousel')['padding' + u.cap(fn.position())]();
        };
        // primary dimension (width or height) value
        calc.primary = function () {
            return fn[keys.posdim[fn.position()]]();
        };
        // position with regard to show/hide
        fn.togglePos = function () {
            return state.isSelectorShown ? fn.basePadding() : fn['margin' + u.cap(fn.position())]() - fn.primary();
        };
        // reach of selector into carousel (with toggle)
        calc.reach = function () {
            return fn[fn.position()]() + fn.primary() + fn.controlDim();
        };

        fn.remodel = function () {
            if (ro.isSelectorDisplayHidden()) {
                fn.style({ display: 'none' });
            } else {
                u.remStyle(fn.node, ['display']);

                fn.style(fn.dims('content'), fn.pos());

                gnc('select').remodel();
                gnc('toggle').remodel();
            }
        };

        return fn.extend(nodes.selector);
    };

    /*
        Content slide node calculator
     */
    fnCalcs.slide = function (fn) {
        fn.remodel = function (slide, index) {
            u.setStyle(slide, u.px(Object.assign({}, fn.dims('content'))));

            gnc('slideContent').remodel(slide.firstChild);
        };

        return fn.extend(u.find('.' + u.cnp('slide'), nodes.slides), gnc('view'));
    };

    /*
        Content node calculator
     */
    fnCalcs.slideContent = fnCalcs.thumbContent = function (fn, name) {
        var item = name.replace(/Content/, '');
        var scaleFn = item + 'ScaleType',
            showFn = 'show' + u.cap(item) + 'Content';
        var parentc = item === 'thumb' ? 'thumbOverlay' : item;
        var resetStyles = ['display', 'width', 'height', 'transform', 'transformOrigin', 'position', 'top', 'left'];

        fn.scale = function () {
            return gnc(parentc).hypotenuse() / gnc('view').hypotenuse() * 3;
        };

        fn.scaleId = function (c) {
            return Object.keys(ro[scaleFn]()).find(function (k) {
                return c.classList.contains(k) || c.tagName.toLowerCase() === k;
            });
        };

        fn.scaleType = function (c) {
            return ro[scaleFn]()[fn.scaleId(c) || 'def'];
        };

        fn.scaleRatio = function (v, m) {
            return Math[m](fn.ratioWidth(v), fn.ratioHeight(v));
        };

        fn.parentPos = function (e) {
            return { scrollTop: fn.parentScrollTop(e), scrollLeft: fn.parentScrollLeft(e) };
        };

        keys.corpos.origin.forEach(function (p) {
            var d = keys.posdim[p],
                cnt = 'content' + u.cap(d);
            // parentScrollTop, parentScrollLeft methods
            fn['parentScroll' + u.cap(p)] = function (c) {
                return Math.floor((fn[d](c) - gnc(parentc)[cnt]()) / 2);
            };
            // width, height methods
            fn[d] = function (c) {
                return parseFloat(u.comStyle(c, d));
            };
            // ratio width and height methods
            fn['ratio' + u.cap(d)] = function (v) {
                return gnc(parentc)[cnt]() / v[d];
            };
        });

        fn.remodel = function (content) {
            if (ro[showFn]()) {
                u.remStyle(content, ['display']);

                var firstChild = content.firstChild,
                    parentNode = content.parentNode;
                // remove any previously applied styles

                [content, firstChild].forEach(function (e) {
                    return u.remStyle(e, resetStyles);
                });

                switch (fn.scaleType(firstChild)) {
                    case 'viewer':
                        var scaleDims = gnc('view').dims('content');
                        u.setStyle(content, u.px({
                            transform: 'scale(' + fn.scale() + ')',
                            transformOrigin: 'center center',
                            width: scaleDims.width,
                            height: scaleDims.height
                        }));
                        u.setStyle(content, u.px({
                            width: scaleDims.width / (fn.width(content) * fn.scale()) * gnc(parentc).contentWidth(),
                            height: scaleDims.height / (fn.height(content) * fn.scale()) * gnc(parentc).contentHeight()
                        }));
                        u.setAttr(parentNode, fn.parentPos(content));
                        break;

                    case 'content-max':
                        var scaleDims = { width: fn.width(firstChild), height: fn.height(firstChild) };
                        u.setStyle(firstChild, u.px({
                            display: 'block',
                            height: fn.scaleRatio(scaleDims, 'max') * scaleDims.height,
                            width: fn.scaleRatio(scaleDims, 'max') * scaleDims.width
                        }));
                        u.setAttr(parentNode, fn.parentPos(firstChild));
                        break;

                    case 'content-min':
                        var scaleDims = { width: fn.width(firstChild), height: fn.height(firstChild) };
                        u.setStyle(firstChild, u.px({
                            position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)',
                            height: fn.scaleRatio(scaleDims, 'min') * scaleDims.height,
                            width: fn.scaleRatio(scaleDims, 'min') * scaleDims.width
                        }));
                        break;
                }
            } else {
                u.setStyle(content, { display: 'none' });
            }
        };

        return fn;
    };

    /*
        Slide container node calculator
     */
    fnCalcs.slides = function (fn) {
        fn.remodel = function () {
            var items = u.array(u.findAll('.' + u.cnp('slide'), nodes.slides));
            // we are dependent on sildes so remodel them first
            items.forEach(function (t, i) {
                gnc('slide').remodel(t, i);
            });

            fn.style(fn.dims(), fn.pos());
        };

        return fn.extend(nodes.slides, gnc('view'));
    };

    /*
        Content thumbnail node calculator
     */
    fnCalcs.thumb = function (fn) {
        var calc = fn.calc;


        keys.corpos.origin.forEach(function (p) {
            var d = keys.posdim[p],
                is = 'is' + u.cap(keys.alias[d]),
                cnt = 'content' + u.cap(d);
            // top, left methods
            fn[p] = function (i) {
                return gnc('selector')[is]() ? i * fn[d]() : 0;
            };
            // width, height methods
            calc[d] = function () {
                return gnc('selector')[is]() ? gnc('select')[cnt]() / gnc('select').viewCount() : gnc('select')[cnt]();
            };
        });

        fn.remodel = function (thumb, index) {
            gnc('thumbOverlay').remodel(thumb.firstChild);

            u.setStyle(thumb, u.px(Object.assign({}, fn.dims('content'))));
        };

        return fn.extend(u.find('.' + u.cnp('thumb'), nodes.thumbs));
    };

    /*
        Thumbnail overlay node calculator
     */
    fnCalcs.thumbOverlay = function (fn) {
        fn.remodel = function (overlay) {
            u.setStyle(overlay, u.px(Object.assign({}, fn.dims('content'), fn.origin())));

            gnc('thumbContent').remodel(overlay.firstChild);
        };

        return fn.extend(u.find('.' + u.cnp('thumb') + '>.' + u.cnp('thumb-overlay'), nodes.thumbs), gnc('thumb'));
    };

    /*
        Thumbnail container node calculator
     */
    fnCalcs.thumbs = function (fn) {
        var calc = fn.calc;
        // is content item count less than thumb view count?

        calc.isUnderViewCount = function () {
            return order.length < gnc('select').viewCount();
        };
        // thumb view count less order length
        calc.viewLessLength = function () {
            return gnc('select').viewCount() - order.length;
        };
        // extent padding from select (for fixing scroll length)
        calc.padFix = function () {
            return gnc('select')['padding' + u.cap(keys.dimpos[gnc('selector').planeDim()][1])]();
        };

        keys.corpos.origin.forEach(function (p) {
            var d = keys.posdim[p],
                is = 'is' + u.cap(keys.alias[d]);
            // top, left methods
            fn[p] = function () {
                if (gnc('selector')[is]()) return fn.isUnderViewCount() ? fn.viewLessLength() * gnc('thumb')[d]() / 2 : -1 * state.firstThumbIndex * gnc('thumb')[d]();

                return 0;
            };
            // width, height methods
            calc[d] = function () {
                if (gnc('selector')[is]()) return Math.ceil(order.length * gnc('thumb')[d]() + fn.padFix());else return gnc('thumb')[d]();
            };
        });

        fn.remodel = function () {
            var items = u.array(u.findAll('.' + u.cnp('thumb'), nodes.thumbs));
            // we are dependent on thumbs so remodel them first
            items.forEach(function (t, i) {
                gnc('thumb').remodel(t, i);
            });

            fn.style(fn.dims());
        };

        return fn.extend(nodes.thumbs);
    };

    /*
        Selector toggle node calculator
     */
    fnCalcs.toggle = function (fn) {
        fn.remodel = function () {
            if (ro.isSelectorDisplayTransient()) {
                u.remStyle(nodes.toggle, ['visibility'].concat(_toConsumableArray(keys.pos)));

                var pos = gnc('selector').position();
                fn.style(_defineProperty({}, pos, gnc('selector')[keys.posdim[pos]]()));
            } else {
                u.setStyle(nodes.toggle, { visibility: 'hidden' });
            }
        };

        return fn.extend(nodes.toggle);
    };

    /*
        View node calculator
     */
    fnCalcs.view = function (fn) {
        fn.remodel = function () {
            gnc('slides').remodel();

            fn.style(fn.dims('content'), fn.origin());
        };

        return fn.extend(nodes.view, gnc('viewer'));
    };

    /*
        View node calculator
     */
    fnCalcs.viewer = function (fn) {
        var calc = fn.calc;


        calc.isVisible = function () {
            return ro.isSelectorDisplayVisible();
        };

        keys.corpos.origin.forEach(function (p) {
            var d = keys.posdim[p],
                is = 'is' + u.cap(keys.alias[d]),
                cnt = 'content' + u.cap(d);
            // top, left methods
            calc[p] = function () {
                return fn.isVisible() && ro.selectorPosition() === p ? gnc('selector')[d]() : 0;
            };
            // width, height methods
            calc[d] = function () {
                return gnc('carousel')[cnt]() - (!fn.isVisible() || gnc('selector')[is]() ? 0 : gnc('selector')[d]());
            };
        });

        fn.remodel = function () {
            if (ro.isViewerDisplayHidden()) {
                fn.style({ display: 'none' });
            } else {
                u.remStyle(fn.node, ['display']);

                gnc('view').remodel();
                gnc('prev').remodel();
                gnc('next').remodel();

                fn.style(fn.dims('content'), fn.origin());
            }
        };

        return fn.extend(nodes.viewer);
    };
    // get node calculator
    var gnc = function gnc(n) {
        return calcs[n] || fnCalcs[n](calcs[n] = basec(), n).cacheExtend();
    };
    // setup all node calculators(
    Object.keys(fnCalcs).forEach(gnc);

    return calcs;
};

/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var u = __webpack_require__(0);

var _require = __webpack_require__(3),
    objectDefaults = _require.objectDefaults,
    enums = _require.enums;

module.exports = function (options, root) {
    var _options$responsive = options.responsive,
        responsive = _options$responsive === undefined ? {} : _options$responsive;
    // responsive labels sorted ascending by value

    var sortedLabels = u.valueSort(responsive);

    /**
        Selects the first valid responsive label in the named config
        option whose corresponding responsive value is less than the
        given dimension of the node container and returns its value.
         If no label is found then 'def' (for default) is returned.
         If no value is found at the determined label then the named
        option value itself is returned.
      */
    var getOptionValue = function getOptionValue(name, dim) {
        var retval = options[name];

        if (retval === null) {
            retval = objectDefaults[name];
        } else if ((typeof retval === 'undefined' ? 'undefined' : _typeof(retval)) === 'object') {
            var rootDim = ro[dim]();
            // intersection of sorted and config option responsive labels
            var optionLabels = sortedLabels.filter(function (i) {
                return typeof retval[i] !== 'undefined';
            });
            // find first valid responsive label or use 'def'
            var label = optionLabels.find(function (i) {
                return rootDim < responsive[i];
            }) || 'def';
            // return option responsive value if not null
            if (typeof retval[label] !== 'undefined') retval = retval[label];
            // merge with defaults
            if (objectDefaults[name]) retval = Object.assign({}, objectDefaults[name], retval);
        }

        return retval;
    };
    // non-responsive and custom config options
    var ro = {
        animator: function animator() {
            return options.animator;
        },
        responsive: function responsive() {
            return options.responsive;
        },
        height: function height() {
            return options.height || parseFloat(u.comStyle(root, 'height'));
        },
        width: function width() {
            return options.width || parseFloat(u.comStyle(root, 'width'));
        },
        // get responsive label for dimension
        label: function label(d) {
            return sortedLabels.find(function (i) {
                return ro[d]() < responsive[i];
            }) || 'def';
        },
        selectorIsShown: function selectorIsShown() {
            return ro.isSelectorDisplayTransient() ? ro.showSelector() : !ro.isSelectorDisplayHidden();
        }
    };
    // add remaining options as responsive functions
    Object.keys(options).forEach(function (k) {
        return ro[k] = ro[k] || function () {
            return getOptionValue(k, 'width');
        };
    });
    // add config option enum functions
    Object.keys(enums).forEach(function (k) {
        return enums[k].forEach(function (v) {
            return ro['is' + u.cap(k) + u.cap(v)] = function () {
                return ro[k]() === v;
            };
        });
    });

    return ro;
};

/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

module.exports = function () {
    var _id = null,
        args = {};
    // get arguments (up to 3 parameters)
    for (var i = 0; i < 3; i++) {
        switch (_typeof(arguments[i])) {
            case 'function':case 'string':
                args.op = arguments[i];break;
            case 'number':
                args.time = arguments[i];break;
            case 'boolean':
                args.repeat = arguments[i];break;
            case 'object':
                args.op = arguments[i].op;
                args.time = arguments[i].wait;
                args.repeat = arguments[i].repeat;
        }
    }
    // determine the timer method
    var doStart = function doStart() {
        return (args.repeat ? setInterval : setTimeout)(args.op, args.time);
    };
    var doClear = function doClear() {
        return (args.repeat ? clearInterval : clearTimeout)(_id);
    };

    var methods = {
        id: function id() {
            return _id;
        },

        // set the function to be executed for timer
        op: function op(_op) {
            args.op = _op;return this;
        },

        // set the delay for timer
        wait: function wait(time) {
            args.time = time;return this;
        },

        // start new timer clearing old id first or if id not set
        start: function start(clear, now) {
            if (clear) this.clear();if (now) args.op();if (!_id) _id = doStart();return this;
        },

        // clear timer and unset id
        clear: function clear() {
            if (_id) doClear(_id);_id = null;return this;
        }
    };

    return methods;
};

/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

/*! event-mixer v0.0.2 @Wed, 12 Apr 2017 18:26:06 GMT */
!function(n,t){ true?module.exports=t():"function"==typeof define&&define.amd?define([],t):"object"==typeof exports?exports.eventMixer=t():n.eventMixer=t()}(this,function(){return function(n){function t(e){if(r[e])return r[e].exports;var u=r[e]={i:e,l:!1,exports:{}};return n[e].call(u.exports,u,u.exports,t),u.l=!0,u.exports}var r={};return t.m=n,t.c=r,t.i=function(n){return n},t.d=function(n,r,e){t.o(n,r)||Object.defineProperty(n,r,{configurable:!1,enumerable:!0,get:e})},t.n=function(n){var r=n&&n.__esModule?function(){return n.default}:function(){return n};return t.d(r,"a",r),r},t.o=function(n,t){return Object.prototype.hasOwnProperty.call(n,t)},t.p="",t(t.s=0)}([function(n,t,r){"use strict";var e=function(n){return n.slice(0,1).toUpperCase()+n.slice(1)},u=function(n,t){return[].concat(n).map(t)};t.mixer=function(n){var r={},o=["on","off"],i=function(u,i){return o.forEach(function(t){return n[t+e(u)]=function(n){return this[t](u,n)}}),r[u]=t.handler(n,u,i)},f=function(t){var u=r[t];return r[t].clear(),r[t]=void 0,o.forEach(function(r){return n[r+e(t)]=void 0}),u},c={create:function(n,t){return i(n,t)},destroy:function(n){return f(n)},event:function(n){return r[n]},has:function(n){return!!r[n]},names:function(){return Object.keys(r)},off:function(n,t){return u(n,function(n){return r[n].remove(t)}),this},on:function(n,t){return u(n,function(n){return r[n].append(t)}),this},target:function(){return n}};return o.forEach(function(t){return n[t]=c[t]}),c},t.handler=function(n,t,r){var e=[],o=function(u){for(var o=!0,i=Object.assign({},r,u),f=0;f<e.length;f++){var c=e[f]({target:n,name:t,data:i})||{};if(c.remove&&(e.splice(f,1),f--),c.prevent&&(o=!1),c.stop)break}return o};return{add:function(n){return this.append(n)},append:function(n){return u(n,this.push),this},clear:function(){return e=[],this},count:function(){return e.length},cut:function(n){return n>=0&&n<e.length?e.splice(n,1)[0]:null},indexOf:function(n){return e.indexOf(n)},fire:function(n){return o(n)},has:function(n){return e.indexOf(n)>=0},pop:function(){return e.pop()},prepend:function(n){return[].concat(n).reverse().map(this.unshift),this},push:function(n){return"function"==typeof n&&e.push(n)},remove:function(n){var t=this;return u(n,function(n){return t.cut(e.indexOf(n))}),this},shift:function(){return e.shift()},unshift:function(n){return"function"==typeof n&&e.unshift(n)}}}}])});

/***/ }),
/* 15 */,
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(16);

module.exports = __webpack_require__(5);

/***/ })
/******/ ]);
});