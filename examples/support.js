/*
    Allows switching of theme css for example pages.

    Inspired by:
    https://www.viget.com/articles/js-201-run-a-function-when-a-stylesheet-finishes-loading
 */
window.themeSwitcher = function(thumbslider, selected)
{
    var themes = ['minimal', 'candybar', 'grayscale', 'greenery'];

    var link = document.head.querySelector('link[title=default]');
    if (!link)
    {
        link = document.createElement('link');

        link.setAttribute('rel', 'stylesheet');
        link.setAttribute('title', 'default');

        document.head.appendChild(link);
    }

    var header = document.createElement('div');
    var loader = document.createElement('img');
    var chooser = document.createElement('select');

    header.setAttribute('style', 'background-color:#333;position:fixed;top:0;right:0;padding:4px;z-index:10;');

    // window.addEventListener('mouseover', function() { header.setAttribute('style', 'display: block;'); });
    // window.addEventListener('mouseout', function() { header.setAttribute('style', 'display: none;'); });

    loader.onerror = function()
    {
        document.body.removeChild(loader);
        thumbslider.names().forEach(function(n) { thumbslider.get(n).remodel(); });
    };

    var updateTheme = function(name)
    {
        var href = '../dist/themes/' + name + '-default.css';

        link.setAttribute('href', href);
        loader.setAttribute('src', href);

        document.body.appendChild(loader);
    };

    chooser.addEventListener('change', function(e) { updateTheme(e.target.value) });

    themes.map(function(name)
    {
        var option = document.createElement('option');

        option.textContent = name;
        option.setAttribute('value', name);

        if (name === selected)
        {
            option.setAttribute('selected', true);
            updateTheme(name);
        }

        return chooser.appendChild(option);
    });

    header.appendChild(chooser);
    document.body.appendChild(header);
}
