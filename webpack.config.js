var fs = require('fs');
var path = require('path');
// var CleanPlugin = require('clean-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var OnBuildPlugin = require('on-build-webpack');
var webpack = require('webpack');
var packson = require('./package.json');
var buildThemes = require('./build.themes');


var moduleDir = __dirname;
var sourceDir = path.join(moduleDir, 'src');
var stylesDir = path.join(sourceDir, 'style');

module.exports = function(env)
{
    var config = {};

    config.entry =
    {
        [packson.name]: path.join(sourceDir, 'index.js'),
        [`${packson.name}.poly`]: path.join(sourceDir, 'index.poly.js')
    }

    config.output =
    {
        path: path.join(moduleDir, 'dist'),
        libraryTarget: 'umd',
        library: packson.name,
        // filename: '[name].js'
    };

    config.resolve = { extensions: ['.js', '.less'] };

    var es6Loader =
    {
        test : /\.js$/,
        include: [sourceDir],
        loader: 'babel-loader',
        options: { presets: ['env'] }
    };
    var lessLoader =
    {
        test: /\.less$/,
        include: [stylesDir],
        loader: ExtractTextPlugin.extract("css-loader!autoprefixer-loader!less-loader")
    };

    config.module = { rules: [ es6Loader, lessLoader ] };

    var bannerPluginOptions =
    {
        // the banner as string, it will be wrapped in a comment
        banner: packson.name + ' v' + packson.version + ' @' + new Date().toUTCString(),
        // if true, banner will not be wrapped in a comment
        raw: false,
        // if true, the banner will only be added to the entry chunks
        entryOnly: true
    };

    config.plugins =
    [
        new webpack.BannerPlugin(bannerPluginOptions),
        // merge css into bundle
        new ExtractTextPlugin({ filename: '[name].min.css', allChunks: false }),
        // post build execution
        new OnBuildPlugin(buildThemes),
        // clean out the output directory
        // new CleanPlugin(['dist/*'], { root: moduleDir, dry: false, verbose: true })
        // ignore certain modules
        new webpack.IgnorePlugin(/mock-browser/)
    ];

    return config;
}
