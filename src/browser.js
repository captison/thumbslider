try
{
    var { mocks: { MockBrowser } } = require('mock-browser');

    exports.document = MockBrowser.createDocument();
    exports.window = MockBrowser.createWindow();
}
catch (e)
{
    exports.document = document;
    exports.window = window;
}
