var polyfiller = require('./polyfiller');


var filled = false, queue = [];
var execute = () => { while (queue.length > 0) queue.shift()(require('./code/factory')); }
var success = () => { filled = true; execute(); }

module.exports = function(callback)
{
    queue.push(callback);

    if (filled)
        execute();
    else
        polyfiller.fill(success, function() { throw arguments; });
}
