
module.exports = function()
{
    var id = null, args = {};
    // get arguments (up to 3 parameters)
    for (var i=0;i<3;i++)
    {
        switch (typeof arguments[i])
        {
            case 'function': case 'string':
                args.op = arguments[i]; break;
            case 'number':
                args.time = arguments[i]; break;
            case 'boolean':
                args.repeat = arguments[i]; break;
            case 'object':
                args.op = arguments[i].op;
                args.time = arguments[i].wait;
                args.repeat = arguments[i].repeat;
        }
    }
    // determine the timer method
    var doStart = () => (args.repeat ? setInterval : setTimeout)(args.op, args.time);
    var doClear = () => (args.repeat ? clearInterval : clearTimeout)(id);

    var methods =
    {
        id() { return id; },
        // set the function to be executed for timer
        op(op) { args.op = op; return this; },
        // set the delay for timer
        wait(time) { args.time = time; return this; },
        // start new timer clearing old id first or if id not set
        start(clear, now) { if (clear) this.clear(); if (now) args.op(); if (!id) id = doStart(); return this; },
        // clear timer and unset id
        clear() { if (id) doClear(id); id = null; return this; }
    }

    return methods;
}
