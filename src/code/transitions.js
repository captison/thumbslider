var u = require('./utils');


exports.fade = (anim, dims, plane, forward) =>
{
    var prev = (node) =>
    {
        anim.events().node(node);

        anim.after(() =>
        {
            u.setStyle(node, { visibility: 'hidden' });
            u.remStyle(node, ['opacity']);
        });

        anim.toStyle({ opacity: 0 })
    }

    var next = (node) =>
    {
        anim.events().node(node);

        u.setStyle(node, { opacity: 0, visibility: 'visible' });

        anim.after(() => { u.remStyle(node, ['opacity']); });

        anim.toStyle({ opacity: 1 });
    }

    return { prev, next };
}

exports.slide = (anim, dims, plane, forward) =>
{
    var prev = exports.unstack(anim, dims, plane, forward).prev;
    var next = exports.stack(anim, dims, plane, forward).next;

    return { prev, next };
}

exports.stack = (anim, dims, plane, forward) =>
{
    var prev = (node) =>
    {
        anim.events().node(node);

        anim.after(() => { u.setStyle(node, { visibility: 'hidden' }); });
        // only tweening here to prevent immediate node disappearance
        anim.toStyle({ top: 0, left: 0 });
    }

    var next = (node) =>
    {
        anim.events().node(node);

        u.setStyle(node, u.px(
        {
            top: plane === 'horizontal' ? 0 : forward ? dims.height : -dims.height,
            left: plane === 'vertical' ? 0 : forward ? dims.width : -dims.width,
            visibility: 'visible',
            zIndex: '1'
        }));

        anim.after(() => { u.remStyle(node, ['z-index']); });

        anim.toStyle(u.px({ top: 0, left: 0 }));
    };

    return { prev, next };
}

exports.unstack = (anim, dims, plane, forward) =>
{
    var prev = (node) =>
    {
        anim.events().node(node);

        u.setStyle(node, { zIndex: '1' });

        anim.after(() =>
        {
            u.setStyle(node, u.px({ top: 0, left: 0, visibility: 'hidden' }));
            u.remStyle(node, ['z-index']);
        });

        anim.toStyle(u.px(
        {
            top: plane === 'horizontal' ? 0 : forward ? -dims.height : dims.height,
            left: plane === 'vertical' ? 0 : forward ? -dims.width : dims.width
        }));
    }

    var next = (node) =>
    {
        u.setStyle(node, { visibility: 'visible' });
    }

    return { prev, next };
}
