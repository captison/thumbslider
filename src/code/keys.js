
var ins = (s,v) => s.replace(/\[x\]/g, v)
var sos = (s) => s.split(/\s+/);

var x = exports;

x.events = sos('rotate toggle');
x.boxmod = sos('margin-[x] border-[x]-width padding-[x]');
x.citem = sos('slide thumb');
x.view = sos('hidden visible transient overlay');
x.mutate = sos('add remove toggle');
x.plane = sos('vertical horizontal');
x.scaleType = sos('content-max content-min viewer none');

x.dimpos = { width: ['left', 'right'], height: ['top', 'bottom'] };
x.corpos = { origin: ['top', 'left'], extent: ['bottom', 'right'] };
// aliasing
x.alias =
{
    width: 'horizontal', height: 'vertical', horizontal: 'width', vertical: 'height',
    prev: 'origin', next: 'extent'
};

// calculated keys
x.dim = Object.keys(x.dimpos);
x.cor = Object.keys(x.corpos);

x.pos = Object.keys(x.corpos).reduce((a,k) => a.concat(x.corpos[k]), []);

x.posdim = x.dim.reduce((a,d) => { x.dimpos[d].forEach(p => a[p] = d); return a; }, {});
x.poscor = x.cor.reduce((a,d) => { x.corpos[d].forEach(p => a[p] = d); return a; }, {});

x.comp = x.cor.reduce((a,c) => { var v = x.corpos[c]; [a[v[0]], a[v[1]]] = [v[1], v[0]]; return a; }, {});

x.box = x.boxmod.reduce((a,m) => { x.pos.forEach(p => a.push(ins(m, p))); return a; }, []);
