var browser = require('../browser');


/*
    Application utility functions.

 */
module.exports =
{
    // add attributes to element
    addAttrs(e, a={}) { for (var n in a) e.setAttribute(n, n === 'class' ? this.cnp(a[n]) : a[n]); return e; },

    // Add html attribute name prefix to `string`
    anp(s) { return s.split(/\s+/).map(n => 'data-' + this.cnp(n)).join(' '); },

    // convert item to array optionally slicing away initial arguments
    array(i, s=0) { return Array.prototype.slice.call(i, s); },

    // capitalize first letter of string
    cap(s) { return s.slice(0,1).toUpperCase() + s.slice(1); },

    // Add class name prefix to `string`
    cnp(s) { return s.split(/\s+/).map(n => this.vars.cnPrefix + n).join(' '); },

    // shallow object copy
    copy(o) { return Object.assign({}, o); },

    // create a new element using tag anme and attributes
    create(t, a={}) { return this.addAttrs(this.vars.document.createElement(t), a); },

    // convert dashed string to camel case
    dashcam(s) { return s.split(/-/).map((e,i) => i > 0 ? this.cap(e) : e).join(''); },

    // remove styles from element
    remStyle(e, a=[]) { a.forEach(s => e.style[this.vars.fnRemove](s)); },

    // find first element in document or given element by selector
    find(s, e) { return (e || this.vars.document).querySelector(s); },

    // find all elements in document or given element by selector
    findAll(s, e) { return (e || this.vars.document).querySelectorAll(s); },

    // get computed style value, clearing inline styles first if requested
    comStyle(e, n, c=false) { if (c) this.remStyle(e, [n]); return this.styleObj(e)[n]; },

    // calculate hypotenuse
    hypo(x, y) { return Math.sqrt((x * x) + (y * y)); },

    // convert number to milliseconds (multiply by 1000)
    ms(n) { return n * 1000; },

    // returns new object with processed keys and values
    process(o, p, v) { return Object.keys(o).reduce((a,k) => { a[p(k)] = v(o[k]); return a; } ,{}) },

    // append 'px' to numeric values
    px(v) { return typeof v === 'number' ? v + 'px' : typeof v === 'object' ? this.pxo(v) : v },

    // append 'px' to numeric values in object
    pxo(o) { if (typeof o === 'object') { for (var v in o) o[v] = this.px(o[v]); return o; } else return this.px(o); },

    // random number from 0 to given number (exclusive)
    random(n) { return Math.floor(Math.random() * n) },

    // remove all selected node children of element
    remAll(s, e) { return this.array(this.findAll(s, e)).map(n => n.parentNode.removeChild(n)); },

    // set attributes on element
    setAttr(e, o={}) { Object.keys(o).forEach(k => e[k] = o[k]); },

    // add styles to element
    setStyle(e, o={}) { Object.keys(o).forEach(k => e.style[k] = o[k]); },

    // shuffle array elements (in place)
    shuffle(a) { for (var i=a.length; i; i--) this.swap(a, i-1, this.random(i-1)); return a; },

    // get style object for element
    styleObj(e) { return this.vars.window.getComputedStyle(e); },

    // swap values in array at given indices
    swap(a, x, y) { [a[x], a[y]] = [a[y], a[x]]; return a; },

    // generate a unique id
    uid() { return (this.vars.inc++).toString(16).toUpperCase(); },

    // keys of object in value order
    valueSort(o) { return Object.keys(o).map(k => [k,o[k]]).sort((a,b) => a[1] - b[1]).map(i => i[0]) },

    vars:
    {
        cnPrefix: 'ts-',
        document: browser.document,
        inc: Math.ceil(Math.random() * 100000),
        fnRemove: browser.document.documentElement.style.removeProperty ? 'removeProperty' : 'removeAttribute',
        window: browser.window,
    }
}
