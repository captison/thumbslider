var tweeners = require('./tweeners');
var u = require('./utils');


module.exports = function()
{
    var args = {}, params = {};
    // get arguments (up to 3 parameters)
    for (var i=0,imx=Math.min(arguments.length,3);i<imx;i++)
    {
        var argument = arguments[i];

        switch (typeof argument)
        {
            case 'function': args.exec = argument; break;
            case 'string': params.name = argument; break;
            case 'number': args.time = argument; break;
            case 'boolean': args.jump = argument; break;
            case 'object': [ args = argument, params = {} ] = [ argument.args, argument.params ]; break;
        }
    }

    var methods =
    {
        // function to run after animation completes
        after(f) { params.after = f; return this; },
        // function to run before animation starts
        before(f) { params.before = f; return this; },
        // clone this animator
        clone() { return module.exports({ args: u.copy(args), params: u.copy(params) }); },
        // function to for each animation frame
        during(f) { params.during = f; return this; },
        // easing value for animation
        ease(e) { params.ease = e; return this; },
        // set/reset all event functions (before, after, during)
        events(b,a,d) { this.before(b); this.after(a); this.during(d); return this; },
        // the function to execute animation
        exec(f) { args.exec = f; return this; },
        // apply jump setting
        jump(b) { args.jump = b; return this; },
        // name for this animator
        name(n) { params.name = n; return this; },
        // the element to be animated
        node(n) { args.node = n; return this; },
        // animation duration
        time(t) { args.time = t; return this; },
        // tween attributes
        toAttr(attrs, jump) { this.to({ attrs }, jump); },
        // tween css styles
        toStyle(styles, jump) { this.to({ styles }, jump); },
        // run animation
        to(items, jump)
        {
            ((jump || args.jump || typeof args.exec !== 'function') ? tweeners.none : args.exec)
                (args.node, args.time, Object.assign({}, params, items));
        }
    }

    return methods;
}
