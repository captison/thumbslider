var Carousel = require('./carousel');
var options = require('./options');
var u = require('./utils');


var carousels = {}, configs = {};
// options merge and check
var check = (opts, defs) => options.check(Object.assign({}, defs || options.defaults, opts));

module.exports =
{
    create(id, opts)
    {
        var elem = u.find(`#${id}`);

        return elem ? carousels[id] = new Carousel(elem, configs[id] = check(opts)).interface : null;
    },

    get(id)
    {
        return carousels[id];
    },

    names()
    {
        return Object.keys(carousels);
    },

    options(id)
    {
        return Object.assign({}, configs[id]);
    },

    update(id, opts)
    {
        return carousels[id].reset(configs[id] = check(opts, configs[id]));
    },

    tweeners: require('./tweeners'),

    transitions: require('./transitions'),
}
