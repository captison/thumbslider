var u = require('./utils');


/*
    JQuery tweener requires JQuery library.
 */
module.exports.jquery = function(node, time, params)
{
    var options =
    {
        complete: params.after,
        duration: time * 1000,
        start: params.before,
        progress: params.during,
        easing: params.ease,
        queue: false
    }

    $(node).animate(Object.assign({}, params.attrs, params.styles), options);
}

/*
    GSAP tweener requires TweenMax and ScrollToPlugin.
 */
module.exports.gsap = function(node, time, params)
{
    // istanbul apparently cannot handle a destructure/rename/default scenario
    // var { styles: args = {}, attrs = {} } = params;
    // reworking above line of code...
    var { styles = {}, attrs = {} } = params, args = styles;

    var scrollTo = () => args.scrollTo = args.scrollTo || {};

    args.onStart = params.before;
    args.onComplete = params.after;
    args.onUpdate = params.during;
    args.ease = params.ease;

    if (typeof attrs.scrollLeft === 'number') scrollTo().x = attrs.scrollLeft;
    if (typeof attrs.scrollTop === 'number') scrollTo().y = attrs.scrollTop;

    TweenLite.to(node, time, args);
}

/*
    Default non-tween function
 */
module.exports.none = function(node, time, params)
{
    if (params.before) params.before();
    u.setAttr(node, params.attrs || {});
    u.setStyle(node, params.styles || {});
    if (params.during) params.during();
    if (params.after) params.after();
}
