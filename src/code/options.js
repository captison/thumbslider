var keys = require('./keys');
var tweeners = require('./tweeners');
var transitions = require('./transitions');


exports.defaults =
{
    // classname to add to active slide/thumb in carousel
    activeClass: 'active',
    // specify function for animation (function(node, time, params)
    animator: tweeners.none,
    // icon classnames for controls
    icons: null,
    // pause rotation on mouseovers?
    pauseOnMouseover: true,
    // setup custom responsive dimension thresholds
    responsive: { xs: 480, sm: 768, md: 1024, lg: 1200 },
    // seconds of rotation animation
    rotateDuration: 0.8,
    // easing value for rotate animation (library specific)
    rotateEasing: null,
    // seconds per rotation
    rotateInterval: 3,
    // seconds to wait before rotation restart after some interaction
    rotateIntermission: 3,
    // number of items to traverse per rotation
    rotateStep: 1,
    // selector display options ('hidden', 'visible', 'transient', 'overlay')
    selectorDisplay: 'visible',
    // seconds to wait before hiding selector ('transient' only)
    selectorHideDelay: 0,
    // position selector at 'top', 'bottom', 'left', or 'right'
    selectorPosition: 'bottom',
    // size (height or width) of selector as percentage of container
    selectorSize: 0.20,
    // show control icons on mouseover
    showIconsOnHover: true,
    // show selector initially when 'transient'?
    showSelector: true,
    // show slide content?
    showSlideContent: true,
    // show thumbnail content?
    showThumbContent: true,
    // shuffle carousel content items
    shuffle: false,
    // slide scaling style
    slideScaleType: { img: 'content-max' },
    // slide transition type
    slideTransition: transitions.slide,
    // element name to open carousel with
    startAt: null,
    // thumb scaling style
    thumbScaleType: { img: 'content-max' },
    // a ratio of secondary over primary dimension for the thumbnail
    thumbSize: 1.0,
    // seconds of selector hide/show animation
    toggleDuration: 0.5,
    // easing value for toggle animation (library specific)
    toggleEasing: null,
    // size (height or width) of toggle control as percentage of selector
    toggleSize: 0.10,
    // viewer display options ('hidden', 'visible')
    viewerDisplay: 'visible',
    // viewer slides scroll in 'vertical' or 'horizontal' direction
    viewerOrientation: 'horizontal',
}
// default all directional icons to 'deficon'
exports.objectDefaults =
{
    icons: keys.pos.reduce((a,p) => { a[p] = 'deficon'; return a; }, {}),
    slideScaleType: { def: 'none' },
    thumbScaleType: { def: 'viewer' }
}
exports.enums =
{
    selectorDisplay: keys.view,
    selectorPosition: keys.pos,
    slideScaleType: keys.scaleType,
    thumbScaleType: keys.scaleType,
    viewerDisplay: keys.view.slice(0, 2),
    viewerOrientation: keys.plane
}

var validation =
{
    message: function(name, value, type)
    {
        var message = `Invalid value (${value}) specified for config option "${name}". `;

        if (exports.enums[name])
            message = message + `Option must be one of ${exports.enums[name].join(', ')}.`;
        else
            message = message + `Option must be of ${type} type.`;

        return message;
    },

    funcs:
    {
        boolean: (v) => typeof v === typeof true,
        fixed: (v,n) => exports.enums[n].indexOf(v) >= 0,
        function: (v) => typeof v === typeof function() {},
        number: (v) => typeof v === typeof 0,
        string: (v) => typeof v === typeof '',
    },

    types:
    {
        boolean: `pauseOnMouseover showSelector showSlideContent showThumbContent shuffle`,
        fixed: `selectorDisplay selectorPosition slideScaleType thumbScaleType viewerOrientation`,
        function: `animator slideTransition`,
        number: `responsive rotateDuration rotateInterval rotateIntermission rotateStep selectorSize thumbSize
            toggleDuration`,
        string: `activeClass`,
    },

    exec: function(key, value, type)
    {
        if (value == null || typeof value !== 'object')
        {
            if (!this.funcs[type](value, key)) throw new Error(this.message(key, value, type));
        }
        else
        {
            for (var k in value) this.exec(key, value[k], type);
        }
    },

    validate: function(options)
    {
        var { types } = this;

        Object.keys(types).forEach(t => types[t].split(/\s+/).forEach(n => this.exec(n, options[n], t)));

        return options;
    }
}

exports.check = validation.validate.bind(validation);
