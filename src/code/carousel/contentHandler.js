var u = require('../utils');


module.exports = function()
{
    var methods =
    {
        slides: {},
        thumbs: {},
        order: [],

        /**
            Determines the element to use as the thumbnail from the
            given `slide`.

            This is resolved by looking for an element in the slide with a
            'data-tslide-thumb' attribute.  If not found, the first non-text
            child found in the slide is used.

            By default, the element found will be fully cloned to avoid it
            being removed from the slide.  To remove the element from the
            slide (prevent cloning) specify 'remove' in the 'data-tsilde-thumb'
            attribute.
         */
        getThumbnail(slide)
        {
            var thumbAttr = u.anp('thumb');
            var thumb = u.find(`[${thumbAttr}]`, slide) || u.find('*', slide) || slide;

            return thumb === slide || thumb.getAttribute(thumbAttr) !== 'remove' ? thumb.cloneNode(true) : thumb;
        },

        index(cnode)
        {
            u.array(cnode.childNodes).forEach(node =>
            {
                if (node.getAttribute) this.insert(node, node.getAttribute(u.anp('name')));
            });

            return this;
        },

        insert(slide, title, index)
        {
            var name = title || u.uid();

            this.slides[name] = this.toContentItem(slide, 'slide', name);
            this.thumbs[name] = this.toContentItem(this.getThumbnail(slide), 'thumb', name);
            // remove from order if existing
            var atIndex = this.order.indexOf(name);
            if (atIndex >= 0) this.order.splice(atIndex, 1);
            // add to order at requested index or at end of order
            var insertAt = typeof index !== 'number' || index > this.order.length ? this.order.length : index;
            this.order.splice(insertAt, 0, name);

            return this;
        },

        toContentItem(elem, type, name)
        {
            // slide or thumb item
            var item = u.create('div', { class: type, [u.anp('name')]: name });
            // overlay layer for thumb only
            var overlay = type === 'thumb' ? item.appendChild(u.create('div', { class: type + '-overlay' })) : item;
            // content container
            var liner = overlay.appendChild(u.create('div', { class: type + '-content' }));
            // add content element to container
            liner.appendChild(elem);

            return item;
        }
    }

    return methods;
}
