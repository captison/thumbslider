var u = require('../utils');
var { objectDefaults, enums } = require('../options');


module.exports = function(options, root)
{
    var { responsive = {} } = options;
    // responsive labels sorted ascending by value
    var sortedLabels = u.valueSort(responsive);

  /**
      Selects the first valid responsive label in the named config
      option whose corresponding responsive value is less than the
      given dimension of the node container and returns its value.

      If no label is found then 'def' (for default) is returned.

      If no value is found at the determined label then the named
      option value itself is returned.
    */
    var getOptionValue = function(name, dim)
    {
        var retval = options[name];

        if (retval === null)
        {
            retval = objectDefaults[name];
        }
        else if (typeof retval === 'object')
        {
            var rootDim = ro[dim]();
            // intersection of sorted and config option responsive labels
            var optionLabels = sortedLabels.filter(i => typeof retval[i] !== 'undefined');
            // find first valid responsive label or use 'def'
            var label = optionLabels.find(i => rootDim < responsive[i]) || 'def';
            // return option responsive value if not null
            if (typeof retval[label] !== 'undefined') retval = retval[label];
            // merge with defaults
            if (objectDefaults[name]) retval = Object.assign({}, objectDefaults[name], retval);
        }

        return retval;
    }
    // non-responsive and custom config options
    var ro =
    {
        animator: () => options.animator,
        responsive: () => options.responsive,
        height: () => options.height || parseFloat(u.comStyle(root, 'height')),
        width: () => options.width || parseFloat(u.comStyle(root, 'width')),
        // get responsive label for dimension
        label: (d) => sortedLabels.find(i => ro[d]() < responsive[i]) || 'def',
        selectorIsShown: () => ro.isSelectorDisplayTransient() ? ro.showSelector() : !ro.isSelectorDisplayHidden()
    }
    // add remaining options as responsive functions
    Object.keys(options).forEach(k => ro[k] = ro[k] || (() => getOptionValue(k, 'width')));
    // add config option enum functions
    Object.keys(enums).forEach(k => enums[k].forEach(v => ro[`is${u.cap(k)}${u.cap(v)}`] = () => ro[k]() === v ));

    return ro;
}
