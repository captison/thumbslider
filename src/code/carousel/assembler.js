var u = require('../utils');

var names =
[
    'carousel', 'viewer', 'selector', 'view', 'select', 'slides', 'thumbs',
    'prev', 'next', 'focus', 'toggle'
];

var icons = ['prev', 'next'];

module.exports = function(nodes)
{
    var methods =
    {
        appendContent(content, shuffle)
        {
            u.remAll('.' + u.cnp('slide'), nodes.slides);
            u.remAll('.' + u.cnp('thumb'), nodes.thumbs);
            // shuffle the content necessary
            if (shuffle) u.shuffle(content.order);
            // add all slides/thumbs to containers
            content.order.forEach(name =>
            {
                nodes.slides.appendChild(content.slides[name]);
                nodes.thumbs.appendChild(content.thumbs[name]);
            });

            return this;
        },

        assemble(root)
        {
            // create all structural nodes
            names.forEach(n => nodes[n] = u.create('div', { class: (icons.indexOf(n) >= 0 ? 'control ' : '') + n }));
            // assembly node structure (arranged below to show hierarchy)
            (nodes.carousel).appendChild
                (nodes.viewer).appendChild
                    (nodes.prev).parentNode.appendChild
                    (nodes.view).appendChild
                        (nodes.slides).parentNode.parentNode.appendChild
                    (nodes.next).parentNode.parentNode.appendChild
                (nodes.selector).appendChild
                    (nodes.select).appendChild
                        (nodes.thumbs).appendChild
                            (nodes.focus).parentNode.parentNode.parentNode.appendChild
                    (nodes.toggle)
            ;
            // create and append all icon nodes
            icons.forEach(n => nodes[`${n}Icon`] = nodes[n].appendChild(u.create('i')));

            if (root)
            {
                u.remAll('*', root);
                root.appendChild(nodes.carousel);
                nodes.root = root;
            }

            return this;
        }
    }

    return methods;
}
