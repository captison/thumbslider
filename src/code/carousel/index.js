var eventMixer = require('event-mixer');

var animator = require('../animator');
var keys = require('../keys');
var timer = require('../timer');
var u = require('../utils');

var assembler = require('./assembler');
var contentHandler = require('./contentHandler');
var nodecs = require('./nodecs');
var responsive = require('./responsive');


var Carousel = function() { this.init.apply(this, arguments); }
Carousel.prototype =
{
    init(elem, options)
    {
        this.root = elem;
        // create timers functionality
        this.timers = this.createTimers();
        // events we are listening to
        this.listeners = this.createEventListeners();
        // create an interface for this carousel
        this.interface = this.createInterface();
        // event manager
        this.em = keys.events.reduce((m,n) => { m.create(n); return m; }, eventMixer.mixer(this.interface));
        // events we will fire ourselves
        this.events = this.em.names().reduce((a,n) => { a[n] = this.em.event(n); return a; }, {});
        // index all content from root node
        this.content = contentHandler().index(this.root);
        // assemble the carousel for all structural nodes
        this.assembler = assembler(this.nodes = {}).assemble(this.root);
        // add event listeners
        this.listen(this.listeners, this.nodes);
        // continue initialization via .reset()
        this.reset(options);
    },

    createAnimators(ro)
    {
        var animators = {}, executor = ro.animator();

        animators.rotate = animator('carouselRotate', executor, ro.rotateDuration()) .ease(ro.rotateEasing());
        animators.toggle = animator('selectorToggle', executor, ro.toggleDuration()) .ease(ro.toggleEasing());

        return animators;
    },

    createEventListeners()
    {
        var listeners =
        {
            carousel:
            {
                mouseEnter: e => { if (this.ro.pauseOnMouseover()) this.state.hovered = true; },
                mouseLeave: e =>
                {
                    if (this.ro.pauseOnMouseover()) { this.state.hovered = false; this.pauseRotation(); }
                }
            },
            next:
            {
                click: e => { this.pauseRotation(); this.moveCurrentIndex(1); }
            },
            prev:
            {
                click: e => { this.pauseRotation(); this.moveCurrentIndex(-1); }
            },
            select:
            {
                mouseOver: e =>
                {
                    u.setStyle(this.nodes.select, { overflow: 'auto' }); this.state.syncSelector = false;
                },
                mouseLeave: e => { u.remStyle(this.nodes.select, ['overflow']); this.state.syncSelector = true; }
            },
            selector:
            {
                mouseOver: e => this.timers.selectorHide.clear(),
                mouseLeave: e => this.toggleHideDelay()
            },
            thumbs:
            {
                click: e => this.onThumbClick(e, e.target)
            },
            toggle:
            {
                click: e => this.toggleSelector()
            },
            window:
            {
                load: e => { this.remodel(); this.setCurrentIndex(undefined, true); },
                resize: e => this.timers.eventRemodel.start(true)
            }
        }

        return listeners;
    },

    createInterface()
    {
        var face =
        {
            currentIndex: () => { return this.state.current.index; },
            getName: () => { return this.root.getAttribute('id'); },
            getSlide: (i) => { return this.itemAt('slide', i); },
            getThumb: (i) => { return this.itemAt('thumb', i); },
            itemCount: () => { return this.content.order.length; },
            remodel: () => { this.remodel(); return face; },
            start: () => { this.startRotation(); return face; },
            stop: () => { this.stopRotation(); return face; },
        }

        return face;
    },

    createTimers()
    {
        var timers =
        {
            eventRemodel: timer(() => this.remodel(), 0),
            resumeRotation: timer(() => this.state.paused = false),
            rotate: timer(() => { if (!this.isSuspended()) this.setCurrentIndex(this.nextIndex()) }, true),
            selectorHide: timer(() => this.toggleSelector(false)),
        }

        return timers;
    },

    isSuspended()
    {
        return this.state.paused || (this.ro.pauseOnMouseover() && this.state.hovered);
    },

    itemAt(type, id)
    {
        var index = typeof id === 'number' ? id : this.content.order.indexOf(id);
        var valid = index >= 0 && index < this.content.order.length;

        return valid ? this.nodes[type + 's'].childNodes[index + (type === 'thumb' ? 1 : 0)] : null;
    },

    listen(listeners, nodes, add=true)
    {
        var action = (add ? 'add' : 'remove') + 'EventListener';

        for (var listenKey in listeners)
        {
            var node = nodes[listenKey] || u.vars[listenKey];

            for (var eventKey in listeners[listenKey])
            {
                node[action](eventKey.toLowerCase(), listeners[listenKey][eventKey])
            }
        }
    },

    /*
        Move carousel `count` number of items from the current index.

        If resulting index is less than 0 then first item is selected.
        If resulting index is more than item count then last item is selected.

     */
    moveCurrentIndex(count)
    {
        var newIndex = this.state.current.index + count;

        if (newIndex < 0) newIndex = 0;
        else if (newIndex >= this.content.order.length) newIndex = this.content.order.length - 1;

        if (this.state.current.index !== newIndex) this.setCurrentIndex(newIndex);
    },

    nextIndex()
    {
        var newIndex = this.state.current.index + this.state.rotateStep;
        // first and last indexes
        var findex = 0, lindex = this.content.order.length - 1;

        if (newIndex < findex)
        {
            this.state.rotateStep *= -1;
            newIndex = findex - newIndex;
        }
        else if (newIndex > lindex)
        {
            this.state.rotateStep *= -1;
            newIndex = lindex - (newIndex - lindex);
        }

        return newIndex;
    },

    onThumbClick(e, target)
    {
        var name = target.getAttribute(u.anp('name'));

        if (name)
        {
            this.pauseRotation();
            this.setCurrentIndex(this.content.order.indexOf(name), false, true);
        }
        else if (e.currentTarget !== target)
        {
            this.onThumbClick(e, target.parentElement);
        }
    },

    /*
        Pauses auto-rotation for the time specified in config options.
     */
    pauseRotation()
    {
        this.state.paused = true;
        this.timers.resumeRotation.wait(u.ms(this.ro.rotateIntermission())).start(true);
    },

    remodel()
    {
        this.calcs.base.remodel();
    },

    /**
        Fully reset this carousel with new config options.
     */
    reset(options)
    {
        // clear all timers
        Object.keys(this.timers).forEach(t => this.timers[t].clear());
        // setup responsive options
        this.ro = responsive(options, this.root);
        // setup some initial state
        this.state =
        {
            content: this.content,
            current: {},
            isSelectorShown: this.ro.selectorIsShown(),
            previous: {},
            rotateStep: this.ro.rotateStep(),
            syncSelector: true
        };
        // add content nodes to carousel
        this.assembler.appendContent(this.content, this.ro.shuffle());
        // for various calculations
        this.calcs = nodecs(this.nodes, this.ro, this.state);
        // create animators functionality
        this.animators = this.createAnimators(this.ro);
        // set the initial content item to be displayed
        this.state.current.index = this.calcs.base.startIndex();

        this.toggleHideDelay();
    },

    /*
        Makes `index` current in the carousel.
     */
    setCurrentIndex(index = this.state.current.index, jump = false, sync = false)
    {
        if (index >= 0 && index < this.content.order.length && this.events.rotate.fire({ nextIndex: index }))
        {
            var { animators: { rotate }, calcs: { focus, select, slide }, nodes, state } = this;
            var { current, previous } = state, { citem } = keys;
            // set the new index
            previous.index = current.index || 0;
            current.index = index;
            // make sure selector is showing current thumb
            state.firstThumbIndex = select.firstThumb(current.index);
            // update previous/current content items
            citem.forEach(c => { previous[c] = current[c]; current[c] = this.itemAt(c, current.index); });
            // content item active class
            var activeClass = this.ro.activeClass();
            // tween the focus element
            rotate.jump(jump).node(nodes.focus)
                .after(() => citem.forEach(c => { if (current[c]) current[c].classList.add(activeClass); }))
                .before(() => citem.forEach(c => { if (previous[c]) previous[c].classList.remove(activeClass); }))
                .toStyle(u.px(focus.origin()));
            // scroll selector conditionally
            if (state.syncSelector || sync) rotate.events().node(nodes.select).toAttr(select.scrollPositions());
            // do slide transition
            this.slideTransition();
        }
    },

    slideTransition()
    {
        var { animators, calcs, state, state: { current, previous } } = this;
        // get transition method and viewer orientation
        var transition = this.ro.slideTransition(), orientation = this.ro.viewerOrientation();
        // determine relative direction of transition
        var forward = current.index > previous.index || (current.index === previous.index && state.rotateStep > 0);
        // get transition functions for previous/next slides
        var { prev, next } = transition(animators.rotate.clone(), calcs.slide.dims(), orientation, forward);
        // transition the new current slide
        if (next && current.slide) next(current.slide);
        // transition previous slide only if different from current
        if (prev && previous.slide && current.index !== previous.index) prev(previous.slide);
    },

    /*
        Initiates auto-rotation if rotation interval > 0.
     */
    startRotation()
    {
        if (this.content.order.length > 0 && this.calcs.base.isAutoRotated())
        {
            this.timers.rotate.wait(u.ms(this.ro.rotateInterval())).start();
        }
    },

    /*
        Stops auto-rotation.
     */
    stopRotation()
    {
        this.timers.rotate.clear();
    },

    toggleHideDelay()
    {
        if (this.ro.isSelectorDisplayTransient())
        {
            var hideDelay = this.ro.selectorHideDelay();

            if (hideDelay > 0)
                this.timers.selectorHide.wait(u.ms(hideDelay)).start(true);
        }
    },

    toggleSelector(display)
    {
        if (this.ro.isSelectorDisplayTransient())
        {
            var { animators, calcs: { selector, prev, next }, nodes, ro, state } = this;
            // if `display` is not boolean then ignore
            var show = typeof display === 'boolean' ? display : !state.isSelectorShown;

            if (this.events.toggle.fire({ shown: state.isSelectorShown, willShow: show }))
            {
                // clear any pending timer
                this.timers.selectorHide.clear();
                // prepare to hide selector if now being shown
                if (show) this.toggleHideDelay();
                // update selector display state
                state.isSelectorShown = show;
                // perform show/hide animation
                animators.toggle.node(nodes.selector).toStyle(u.px({ [selector.position()]: selector.togglePos() }));
            }
        }
    }
}

module.exports = Carousel;
