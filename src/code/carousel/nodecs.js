var u = require('../utils');
var keys = require('../keys');


module.exports = function(nodes, ro, state)
{
    var { content: { order } } = state, calcs = {};
    // base node calculator functionality
    var basec = function()
    {
        var nodec = { vars: {}, last: {}, calc: {} }, mCache = {};
        // clear calculator cache
        nodec.clear = () => { nodec.last = nodec.vars; nodec.vars = {}; };
        // extend the calculator
        nodec.extend = (node, parentc) =>
        {
            var typeDim = (d,t,e='') => nodec[t+u.cap(keys.dimpos[d][0])+e]() + nodec[t+u.cap(keys.dimpos[d][1])+e]();

            nodec.node = node; nodec.cl = node.classList;
            // individual functions for margins, border-width, and padding
            keys.box.forEach(k => mCache[u.dashcam(k)] = () => parseFloat(u.comStyle(nodec.node, k, true)));
            // default top, left, bottom, right functions
            keys.pos.forEach(k => mCache[k] = () => parentc ? parentc['padding' + u.cap(k)]() : 'auto');

            keys.dim.forEach(d =>
            {
                  var dc = u.cap(d);

                  mCache['margins' + dc] = () => typeDim(d, 'margin');
                  mCache['borders' + dc] = () => typeDim(d, 'border', 'Width');
                  mCache['padding' + dc] = () => typeDim(d, 'padding');
                  mCache['padders' + dc] = () => nodec['padding' + dc]() + nodec['borders' + dc]();
                  // border box and full dims; one must be overriden if no parent!
                  mCache['border' + dc] = () => nodec[d]() - nodec['margins' + dc]();
                  if (parentc)
                      mCache[d] = () => parentc['content' + dc]();
                  else
                      mCache[d] = () => nodec['border' + dc]() + nodec['margins' + dc]();
                  // content box dimensions
                  mCache['content' + dc] = () => nodec['border' + dc]() - nodec['padders' + dc]();
            });

            nodec.dims = (t) => keys.dim.reduce((a,d) => { a[d] = nodec[t?t+u.cap(d):d](); return a; }, {});

            nodec.object = (l) => l.reduce((a,p) => { a[p] = nodec[p](); return a; }, {});

            nodec.origin = () => nodec.object(keys.corpos.origin);

            nodec.pos = () => nodec.object(keys.pos);

            keys.mutate.forEach(k => nodec[`${k}Class`] = function() { nodec.cl[k].apply(nodec.cl, arguments); });

            nodec.updateClass = (n) => { nodec.removeClass(nodec.last[n]); nodec.addClass(nodec[n]()); };

            nodec.style = function() { u.setStyle(nodec.node, u.px(Object.assign.apply(u, arguments))); }

            nodec.attrib = function() { u.setAttr(nodec.node, Object.assign.apply(u, arguments)); }

            nodec.hypotenuse = () => u.hypo(nodec.contentWidth(), nodec.contentHeight());

            return nodec;
        }

        nodec.cacheExtend = () =>
        {
            // create caching methods
            Object.keys(Object.assign(mCache, nodec.calc)).forEach(k =>
            {
                nodec[k] = nodec[k] || (() => nodec.vars[k] = nodec.vars[k] || mCache[k]());
            });

            return nodec;
        }

        return nodec;
    }

    var fnCalcs = {};

    /*
        Base carousel information calculator
     */
    fnCalcs.base = (fn) =>
    {
        var { calc } = fn;
        // is carousel auto rotated?
        calc.isAutoRotated = () => ro.rotateInterval() >= 0 && ro.rotateInterval() >= ro.rotateDuration();
        // orientation of selector ('horizontal' or 'vertical')
        calc.selectorOrientation = () => keys.alias[keys.posdim[keys.comp[ro.selectorPosition()]]];
        // carousel starting index
        calc.startIndex = () =>
        {
            var startAt = ro.startAt();
            var index = typeof startAt === 'number' ? startAt : order.indexOf(startAt);
            if (index < 0) index = order.length + index;
            return (index >= 0 && index < order.length) ? index : 0;
        }
        // full carousel cache reset and remodel
        fn.remodel = () =>
        {
            Object.keys(fnCalcs).forEach(n => gnc(n).clear());
            gnc('carousel').remodel();
        };

        return fn;
    };

    /*
        Carousel node calculator
     */
    fnCalcs.carousel = (fn) =>
    {
        var { calc } = fn;
        // width, height methods
        keys.dim.forEach(d => calc[`border${u.cap(d)}`] = () => ro[d]());

        calc.widthSizeClass = () => u.cnp(`width-${ro.label('width')}`);
        calc.heightSizeClass = () => u.cnp(`height-${ro.label('height')}`);
        calc.selectorDisplayClass = () => u.cnp(`selector-${ro.selectorDisplay()}`);
        calc.selectorPositionClass = () => u.cnp(`selector-${ro.selectorPosition()}`);
        calc.selectorOrientationClass = () => u.cnp(`selector-${gnc('base').selectorOrientation()}`);
        calc.viewerDisplayClass = () => u.cnp(`viewer-${ro.viewerDisplay()}`);

        fn.remodel = () =>
        {
            fn.updateClass('widthSizeClass');
            fn.updateClass('heightSizeClass');
            fn.updateClass('selectorDisplayClass');
            fn.updateClass('viewerDisplayClass');
            fn.toggleClass('hide-icons', ro.showIconsOnHover());

            if (!ro.isSelectorDisplayHidden())
            {
                fn.updateClass('selectorPositionClass');
                fn.updateClass('selectorOrientationClass');
            }

            gnc('selector').remodel();
            gnc('viewer').remodel();

            fn.style(fn.dims('content'));
        }

        return fn.extend(nodes.carousel);
    };

    /*
        Selector focus node calculator
     */
    fnCalcs.focus = (fn) =>
    {
        var { calc } = fn;
        // top, left methods
        keys.corpos.origin.forEach(p => fn[p] = () => gnc('thumb')[p](state.current.index));
        // width, height methods
        keys.dim.forEach(d => calc[d] = () => gnc('thumb')[d]());

        fn.remodel = () => fn.style(fn.dims('content'), fn.origin());

        return fn.extend(nodes.focus, gnc('thumb'));
    };

    /*
        Content item navigation node calculator
     */
    fnCalcs.next = fnCalcs.prev = (fn, name) =>
    {
        var { calc } = fn;

        var corner = keys.corpos[keys.alias[name]];
        // initial distance from edge of caontainer
        calc.initPos = () => parseFloat(u.comStyle(fn.node, fn.position(), true));
        // position of this control
        calc.position = () => keys.dimpos[keys.alias[ro.viewerOrientation()]].find(p => corner.indexOf(p) >= 0);
        // is control position same as selector position?
        calc.withSelector = () => fn.position() === ro.selectorPosition();
        // is a position adjustment needed?
        calc.posAdjustNeeded = () => ro.isSelectorDisplayTransient() && fn.withSelector();
        // totoal offset value for selector and its toggle control
        calc.offset = () => gnc('selector').primary() + gnc('selector').controlDim();

        fn.remodel = () =>
        {
            var { firstChild: { classList } } = fn.node;

            var dim = keys.alias[ro.viewerOrientation()], icons = ro.icons();
            // remove all classes from icon child
            fn.node.firstChild.setAttribute('class', '');
            // set correct css classes for this control
            corner.forEach(p =>
            {
                var add = dim === keys.posdim[p];
                // set directional class name
                fn.toggleClass(u.cnp(p), add);
                // set icon class name(s)
                if (add) classList.add.apply(classList, icons[p].split(/\s+/));
            });

            // remove any prevously set position styles
            u.remStyle(fn.node, corner);
        }

        return fn.extend(nodes[name]);
    };

    /*
        Select node calculator
     */
    fnCalcs.select = (fn) =>
    {
        var { calc } = fn;
        // primary/secondary dimensions of selector ('width' or 'height')
        calc.primaryDim = () => keys.posdim[gnc('selector').position()];
        calc.secondaryDim = () => keys.posdim[keys.comp[gnc('selector').position()]];
        // number of thumbs in select view
        calc.viewCount = () => Math.round(fn[fn.secondaryDim()]() / (fn[fn.primaryDim()]() * ro.thumbSize()));
        // middle thumbnail in view
        calc.middleThumb = () => Math.floor(fn.viewCount() / 2);
        // index of the maximum first viewable thumb
        calc.maxFirstThumb = () => Math.max(order.length - fn.viewCount(), 0);
        // offset of the first viewable thumb based on rotate direction (0 or 1)
        fn.firstOffset = () => fn.viewCount() % 2 == 0 && state.rotateStep < 0 ? 1 : 0;
        // first viewable thumb for given item index
        fn.firstThumb = (i) => Math.min(Math.max(i - fn.middleThumb() + fn.firstOffset(), 0), fn.maxFirstThumb());
        // scroll positions ('scrollTop' and 'scrollLeft')
        fn.scrollPositions = () => u.process(gnc('thumbs').pos(), n => 'scroll' + u.cap(n), Math.abs);

        fn.remodel = () =>
        {
            gnc('thumbs').remodel();
            gnc('focus').remodel();

            fn.style(fn.dims('content'), fn.origin());
            fn.attrib({}, fn.scrollPositions());
        }

        return fn.extend(nodes.select, gnc('selector'));
    };

    /*
        Selector node calculator
     */
    fnCalcs.selector = (fn) =>
    {
        var { calc } = fn;
        // cache selector position as its used a lot here
        calc.position = () => ro.selectorPosition();
        // selector orientation dimension
        calc.planeDim = () => keys.posdim[keys.comp[fn.position()]];

        keys.dim.forEach(d =>
        {
            var is = `is${u.cap(keys.alias[d])}`, cnt = `content${u.cap(d)}`;
            // isHorizontal, isVertical methods
            calc[is] = () => keys.dimpos[d].indexOf(fn.position()) < 0;
            // width, height methods
            calc[d] = () => fn[is]() ? gnc('carousel')[cnt]() : Math.floor(ro.selectorSize() * gnc('carousel')[cnt]());
        });

        keys.cor.forEach(d =>
        {
            var is = `is${u.cap(d)}`;
            // add diagonal boolean methods ('isOrigin', 'isExtent')
            calc[is] = () => keys.poscor[fn.position()] === d;
            // add position methods ('top', 'left', 'bottom', 'right')
            keys.corpos[d].forEach(p => calc[p] = () => !fn[is]() ? `auto` :
                fn.position() === p ? fn.togglePos() : gnc('carousel')[`padding${u.cap(p)}`]());
        });
        // content padding at position of selector
        calc.basePadding = () => gnc('carousel')[`padding${u.cap(fn.position())}`]();
        // primary dimension (width or height) value
        calc.primary = () => fn[keys.posdim[fn.position()]]();
        // position with regard to show/hide
        fn.togglePos = () =>
        {
            return state.isSelectorShown ? fn.basePadding() : fn[`margin${u.cap(fn.position())}`]() - fn.primary();
        }
        // reach of selector into carousel (with toggle)
        calc.reach = () => fn[fn.position()]() + fn.primary() + fn.controlDim();

        fn.remodel = () =>
        {
            if (ro.isSelectorDisplayHidden())
            {
                fn.style({ display: 'none' });
            }
            else
            {
                u.remStyle(fn.node, [ 'display' ]);

                fn.style(fn.dims('content'), fn.pos());

                gnc('select').remodel();
                gnc('toggle').remodel();
            }
        }

        return fn.extend(nodes.selector);
    };

    /*
        Content slide node calculator
     */
    fnCalcs.slide = (fn) =>
    {
        fn.remodel = (slide, index) =>
        {
            u.setStyle(slide, u.px(Object.assign({}, fn.dims('content'))));

            gnc('slideContent').remodel(slide.firstChild);
        }

        return fn.extend(u.find('.' + u.cnp('slide'), nodes.slides), gnc('view'));
    };

    /*
        Content node calculator
     */
    fnCalcs.slideContent = fnCalcs.thumbContent = (fn, name) =>
    {
        var item = name.replace(/Content/, '');
        var scaleFn = `${item}ScaleType`, showFn = `show${u.cap(item)}Content`;
        var parentc = item === 'thumb' ? 'thumbOverlay' : item;
        var resetStyles = ['display', 'width', 'height', 'transform', 'transformOrigin', 'position', 'top', 'left'];

        fn.scale = () => gnc(parentc).hypotenuse() / gnc('view').hypotenuse() * 3;

        fn.scaleId = (c) => Object.keys(ro[scaleFn]()).find(k => c.classList.contains(k) || c.tagName.toLowerCase() === k);

        fn.scaleType = (c) => ro[scaleFn]()[fn.scaleId(c) || 'def'];

        fn.scaleRatio = (v,m) => Math[m](fn.ratioWidth(v), fn.ratioHeight(v));

        fn.parentPos = (e) => ({ scrollTop: fn.parentScrollTop(e), scrollLeft: fn.parentScrollLeft(e)  });

        keys.corpos.origin.forEach(p =>
        {
            var d = keys.posdim[p], cnt = `content${u.cap(d)}`;
            // parentScrollTop, parentScrollLeft methods
            fn[`parentScroll${u.cap(p)}`] = (c) => Math.floor((fn[d](c) - gnc(parentc)[cnt]()) / 2);
            // width, height methods
            fn[d] = (c) => parseFloat(u.comStyle(c, d));
            // ratio width and height methods
            fn[`ratio${u.cap(d)}`] = (v) => gnc(parentc)[cnt]() / v[d];
        });

        fn.remodel = (content) =>
        {
            if (ro[showFn]())
            {
                u.remStyle(content, ['display']);

                var { firstChild, parentNode } = content;
                // remove any previously applied styles
                [content, firstChild].forEach(e => u.remStyle(e, resetStyles));

                switch (fn.scaleType(firstChild))
                {
                    case 'viewer':
                        var scaleDims = gnc('view').dims('content');
                        u.setStyle(content, u.px(
                        {
                            transform: `scale(${fn.scale()})`,
                            transformOrigin: 'center center',
                            width: scaleDims.width,
                            height: scaleDims.height,
                        }));
                        u.setStyle(content, u.px(
                        {
                            width: scaleDims.width / (fn.width(content) * fn.scale()) * gnc(parentc).contentWidth(),
                            height: scaleDims.height / (fn.height(content) * fn.scale()) * gnc(parentc).contentHeight(),
                        }));
                        u.setAttr(parentNode, fn.parentPos(content));
                        break;

                    case 'content-max':
                        var scaleDims = { width: fn.width(firstChild), height: fn.height(firstChild) };
                        u.setStyle(firstChild, u.px(
                        {
                            display: 'block',
                            height: fn.scaleRatio(scaleDims, 'max') * scaleDims.height,
                            width: fn.scaleRatio(scaleDims, 'max') * scaleDims.width
                        }));
                        u.setAttr(parentNode, fn.parentPos(firstChild));
                        break;

                    case 'content-min':
                        var scaleDims = { width: fn.width(firstChild), height: fn.height(firstChild) };
                        u.setStyle(firstChild, u.px(
                        {
                            position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)',
                            height: fn.scaleRatio(scaleDims, 'min') * scaleDims.height,
                            width: fn.scaleRatio(scaleDims, 'min') * scaleDims.width
                        }));
                        break;
                }
            }
            else
            {
                u.setStyle(content, { display: 'none' });
            }
        }

        return fn;
    };

    /*
        Slide container node calculator
     */
    fnCalcs.slides = (fn) =>
    {
        fn.remodel = () =>
        {
            var items = u.array(u.findAll('.' + u.cnp('slide'), nodes.slides))
            // we are dependent on sildes so remodel them first
            items.forEach((t,i) => { gnc('slide').remodel(t, i); });

            fn.style(fn.dims(), fn.pos());
        }

        return fn.extend(nodes.slides, gnc('view'));
    };

    /*
        Content thumbnail node calculator
     */
    fnCalcs.thumb = (fn) =>
    {
        var { calc } = fn;

        keys.corpos.origin.forEach(p =>
        {
            var d = keys.posdim[p], is = `is${u.cap(keys.alias[d])}`, cnt = `content${u.cap(d)}`;
            // top, left methods
            fn[p] = (i) => gnc('selector')[is]() ? i * fn[d]() : 0;
            // width, height methods
            calc[d] = () =>
            {
                return gnc('selector')[is]() ? gnc('select')[cnt]() / gnc('select').viewCount() : gnc('select')[cnt]();
            }
        });

        fn.remodel = (thumb, index) =>
        {
            gnc('thumbOverlay').remodel(thumb.firstChild);

            u.setStyle(thumb, u.px(Object.assign({}, fn.dims('content'))));
        }

        return fn.extend(u.find('.' + u.cnp('thumb'), nodes.thumbs));
    };

    /*
        Thumbnail overlay node calculator
     */
    fnCalcs.thumbOverlay = (fn) =>
    {
        fn.remodel = (overlay) =>
        {
            u.setStyle(overlay, u.px(Object.assign({}, fn.dims('content'), fn.origin())));

            gnc('thumbContent').remodel(overlay.firstChild);
        }

        return fn.extend(u.find('.' + u.cnp('thumb') + '>.' + u.cnp('thumb-overlay'), nodes.thumbs), gnc('thumb'));
    };

    /*
        Thumbnail container node calculator
     */
    fnCalcs.thumbs = (fn) =>
    {
       var { calc } = fn;
       // is content item count less than thumb view count?
       calc.isUnderViewCount = () => order.length < gnc('select').viewCount();
       // thumb view count less order length
       calc.viewLessLength = () => gnc('select').viewCount() - order.length;
       // extent padding from select (for fixing scroll length)
       calc.padFix = () => gnc('select')[`padding${u.cap(keys.dimpos[gnc('selector').planeDim()][1])}`]();

       keys.corpos.origin.forEach(p =>
       {
           var d = keys.posdim[p], is = `is${u.cap(keys.alias[d])}`;
           // top, left methods
           fn[p] = () =>
           {
               if (gnc('selector')[is]())
                   return fn.isUnderViewCount() ? fn.viewLessLength() * gnc('thumb')[d]() / 2 :
                       -1 * state.firstThumbIndex * gnc('thumb')[d]();

               return 0;
           }
           // width, height methods
           calc[d] = () =>
           {
               if (gnc('selector')[is]())
                   return Math.ceil((order.length * gnc('thumb')[d]()) + fn.padFix());
               else
                   return gnc('thumb')[d]();
           }
       });

       fn.remodel = () =>
       {
           var items = u.array(u.findAll('.' + u.cnp('thumb'), nodes.thumbs));
           // we are dependent on thumbs so remodel them first
           items.forEach((t,i) => { gnc('thumb').remodel(t, i); });

           fn.style(fn.dims());
       }

       return fn.extend(nodes.thumbs);
    };

    /*
        Selector toggle node calculator
     */
    fnCalcs.toggle = (fn) =>
    {
        fn.remodel = () =>
        {
            if (ro.isSelectorDisplayTransient())
            {
                u.remStyle(nodes.toggle, ['visibility', ...keys.pos]);

                var pos = gnc('selector').position();
                fn.style({ [pos]: gnc('selector')[keys.posdim[pos]]() });
            }
            else
            {
                u.setStyle(nodes.toggle, { visibility: 'hidden' });
            }
        }

        return fn.extend(nodes.toggle);
    };

    /*
        View node calculator
     */
    fnCalcs.view = (fn) =>
    {
        fn.remodel = () =>
        {
            gnc('slides').remodel();

            fn.style(fn.dims('content'), fn.origin());
        }

        return fn.extend(nodes.view, gnc('viewer'));
    };

    /*
        View node calculator
     */
    fnCalcs.viewer = (fn) =>
    {
        var { calc } = fn;

        calc.isVisible = () => ro.isSelectorDisplayVisible();

        keys.corpos.origin.forEach(p =>
        {
            var d = keys.posdim[p], is = `is${u.cap(keys.alias[d])}`, cnt = `content${u.cap(d)}`;
            // top, left methods
            calc[p] = () => fn.isVisible() && ro.selectorPosition() === p ? gnc('selector')[d]() : 0;
            // width, height methods
            calc[d] = () => gnc('carousel')[cnt]() - (!fn.isVisible() || gnc('selector')[is]() ? 0 : gnc('selector')[d]());
        })

        fn.remodel = () =>
        {
            if (ro.isViewerDisplayHidden())
            {
                fn.style({ display: 'none' });
            }
            else
            {
                u.remStyle(fn.node, [ 'display' ]);

                gnc('view').remodel();
                gnc('prev').remodel();
                gnc('next').remodel();

                fn.style(fn.dims('content'), fn.origin());
            }
        }

        return fn.extend(nodes.viewer);
    };
    // get node calculator
    var gnc = n => calcs[n] || fnCalcs[n](calcs[n] = basec(), n).cacheExtend();
    // setup all node calculators(
    Object.keys(fnCalcs).forEach(gnc);

    return calcs;
}
