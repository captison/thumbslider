var browser = require('./browser');


/*
    Polyfills for Thumbslider.

    These should be loaded through Polyfill.io CDN or similar before a carousel
    instance is created.

    Polyfill list derived from:
    https://polyfill.io/v2/docs/features/

    Example usage:
    https://cdn.polyfill.io/v2/polyfill.js?features=<polyfills>
*/
var features =
`
    Array.isArray
    Array.prototype.filter
    Array.prototype.find
    Array.prototype.forEach
    Array.prototype.indexOf
    Array.prototype.map
    Array.prototype.reduce
    document.querySelector
    Element.prototype.classList
    getComputedStyle
    Object.assign
    Object.keys
    Symbol
`;

exports.features = features.split(/\s+/);

exports.fill = function(ready, error)
{
    var script = browser.document.createElement('script');

    script.src = 'https://cdn.polyfill.io/v2/polyfill.js?features=' + exports.features.join(',');
    script.onload = ready;
    script.onerror = error;

    browser.document.head.appendChild(script);
}
