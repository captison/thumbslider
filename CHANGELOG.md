## Thumbslider Change Log

### In Development (TODOs)

- linked carousels

### Releases

#### 0.0.4

- transitions for slides added
    - fade
    - slide (default & original)
    - stack
    - unstack
- viewer can now be hidden
- event emitter 'event-mixer' added to project
- carousel events added
    - rotate: use .onRotate()/.offRotate()
    - toggle: use .onToggle()/.offToggle()
- testing
    - added jasmine for automated tests
    - added mock-browser to mock browser objects for testing
    - istanbul added for code coverage analysis
- removed proxy methods for Object.keys() and Object.assign()

#### 0.0.3

- fixed issue with bower config

#### 0.0.1

- first version of **Thumbslider**!
